//Fake Player Platform Demonstration Module
//by Cheese

#include "asss.h"
#include "fake.h"

#include "fakeweapons.h"
#include "fakeenergy.h"
#include "fakemove.h"

local Imodman *mm;
local Ilogman *lm;
local Ifake *fake;

local Ifakeweapons *fw;
local Ifakeenergy *fe;
local Ifakemove *fm;


Player *fp=NULL; //fake player handle
Player *fp2=NULL; //fake player handle
int w1=0; //weapon 1 id
int w2=0; //weapon 2 id
int w3=0; //weapon 3 id


void init(Arena *a)
{
	fp=fake->CreateFakePlayer("FPP-Test-1",a,SHIP_WARBIRD,1); //create fake player in asss core
	
	fw->AddFake(fp); //registers fake player with weapons module, required to send position packets
	fe->AddFake(fp); //registers fake player with energy module
	fm->AddFake(fp); //registers fake player with movement module
	
	fe->ChangeFake(fp,2000,500,600,600,50); //energy, recharge, spawns randomly in center
	fe->SetState(fp,STATE_TAKEDAMAGE | STATE_IGNORETEAMDAMAGE | STATE_IGNORESELFDAMAGE,TRUE); //set state flags
	
	fm->ChangeFake(fp,1500,250); //speed, thrust
	fm->SetState(fp,STATE_IGNOREWALLS,TRUE); //set state flags
	
	fw->ChangeFake(fp,SHIP_WARBIRD,1,300,300,0); //set ship, freq, initial position, alivetime
	
	
	
	fp2=fake->CreateFakePlayer("FPP-Test-2",a,SHIP_JAVELIN,2); //create fake player in asss core
	
	fw->AddFake(fp2); //registers fake player with weapons module, required to send position packets
	fe->AddFake(fp2); //registers fake player with energy module
	fm->AddFake(fp2); //registers fake player with movement module
	
	fe->ChangeFake(fp2,1400,400,400,400,50); //energy, recharge, spawns randomly in center
	fe->SetState(fp2,STATE_TAKEDAMAGE | STATE_IGNORETEAMDAMAGE | STATE_IGNORESELFDAMAGE,TRUE); //set state flags
	
	fm->ChangeFake(fp2,2000,150); //speed, thrust
	fm->SetState(fp2,STATE_IGNOREWALLS,TRUE); //set state flags
	
	fw->ChangeFake(fp2,SHIP_JAVELIN,2,700,700,0); //set ship, freq, initial position, alivetime
	
	
	
	//set up weapons
	w1=fw->AddWeapon(); //create weapon 1
	w2=fw->AddWeapon(); //create weapon 2
	w3=fw->AddWeapon(); //create weapon 3
	fw->ChangeWeapon(w1,W_NULL,0,0,0,FALSE,FALSE,100,20,TRUE); //null weapon shared by both fake players to make them "look" at targets
	fw->AssignWeapon(fp,w1,TRUE);
	fw->AssignWeapon(fp2,w1,TRUE);
	fw->ChangeWeapon(w2,W_PROXBOMB,0,10,1,TRUE,FALSE,50,75,TRUE); //lv1 prox bomb, 10 lv2 shrap
	fw->AssignWeapon(fp2,w2,TRUE);
	fw->ChangeWeapon(w3,W_BULLET,1,0,0,FALSE,TRUE,75,50,TRUE); //lv2 nonbounce multifire bullets
	fw->AssignWeapon(fp,w3,TRUE);
	
	
	
	//set initial move targets
	fm->SetMoveTarget(fp,500,500);
	fm->SetMoveTarget(fp2,450,450);
	
}

void unload(Arena *a)
{
	fe->RemoveFake(fp); //removes fake player from energy module
	fm->RemoveFake(fp); //removes fake player from movement module
	fw->RemoveFake(fp); //removes fake player from weapons module
	
	fe->RemoveFake(fp2); //removes fake player from energy module
	fm->RemoveFake(fp2); //removes fake player from movement module
	fw->RemoveFake(fp2); //removes fake player from weapons module
	
	fake->EndFaked(fp); //destroy fake player
	fake->EndFaked(fp2); //destroy fake player
}


void TargetReached(Player *p, int x, int y)
{
	if(p == fp) //set up simple intersecting patrol boxes
	{
		if((x == 500) && (y == 500)) fm->SetMoveTarget(p,550,500);
		if((x == 550) && (y == 500)) fm->SetMoveTarget(p,550,550);
		if((x == 550) && (y == 550)) fm->SetMoveTarget(p,500,550);
		if((x == 500) && (y == 550)) fm->SetMoveTarget(p,500,500);
	}
	else if(p == fp2)
	{
		if((x == 450) && (y == 450)) fm->SetMoveTarget(p,450,525);
		if((x == 450) && (y == 525)) fm->SetMoveTarget(p,525,525);
		if((x == 525) && (y == 525)) fm->SetMoveTarget(p,525,450);
		if((x == 525) && (y == 450)) fm->SetMoveTarget(p,450,450);
	}
}


EXPORT const char info_fppdemo[]=
".\n"
"Fake Player Platform Demonstration Module\n"
"by Cheese\n"
"A basic demonstration of the capabilities of the Fake Player Platform.";

EXPORT int MM_fppdemo(int action, Imodman *mm2, Arena *a)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN,ALLARENAS);
		fake=mm->GetInterface(I_FAKE,ALLARENAS);
		fw=mm->GetInterface(I_FAKEWEAPONS,ALLARENAS);
		fe=mm->GetInterface(I_FAKEENERGY,ALLARENAS);
		fm=mm->GetInterface(I_FAKEMOVE,ALLARENAS);
		
		if(!lm || !fake || !fw || !fe || !fm) //check to make sure dependencies exist
		{
			if(lm) lm->Log(L_ERROR,"<fppdemo> Fake Player Platform Demonstration Module is missing dependencies.");
			return MM_FAIL;
		}
		
		lm->Log(L_ERROR,"<fppdemo> Fake Player Platform Demonstration Module has loaded.");

		return MM_OK;
	}
	else if(action == MM_ATTACH)
	{
		lm->LogA(L_ERROR,"fppdemo",a,"Fake Player Platform Demonstration Module has attached to the arena.");
		
		mm->RegCallback(CB_TARGETREACHED,TargetReached,a); //listen for fake players reaching their destinations
		
		init(a);

		return MM_OK;
	}
	else if(action == MM_DETACH)
	{
		lm->LogA(L_ERROR,"fppdemo",a,"Fake Player Platform Demonstration Module has detached from the arena.");
		
		unload(a);
		
		mm->UnregCallback(CB_TARGETREACHED,TargetReached,a); //stop listening

		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		lm->Log(L_ERROR,"<fppdemo> Fake Player Platform Demonstration Module has unloaded.");

		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(fake);
		mm->ReleaseInterface(fw);
		mm->ReleaseInterface(fe);
		mm->ReleaseInterface(fm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
