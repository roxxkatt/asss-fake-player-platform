#ifndef __FAKEDAMAGE_H
#define __FAKEDAMAGE_H

#include "linuxdeps.h"

/** Called when a fake player takes damage. 
  * dist is how far from the center of the player the bomb exploded (always 0 with bullets). 
  * firedBy is the player which fired the weapon. 
  * damage is the amount of damage taken.
  * wtype is the weapon type that did the damage.
  * bouncing is set when the bomb is a bouncing bomb or bouncing bullet.
  * if the bomb was an EMP bomb, emptime tells you how long the emp lasts (otherwise it is 0).
  * this MAY not be the actual emp time if the previous emp hit lasts longer.
  */
typedef void (*FakeDamageFunc)(Player *fake, Player *firedBy, int dist, int damagedealt, int wtype, int level, BOOL bouncing, int emptime, void *clos);

/** Called when a tile in a tracked region takes damage. x and y is the pixel location,
 * firedBy is the player which fired the weapon, damage is the amount of damage taken,
 * wtype is the type of the weapon that did the damage, see packets/ppk.h for weapon types.
 * if bouncingBomb is set, the bomb exploded after the last bounce
 * if the bomb was an EMP bomb, empTime tells you how long the emp lasts in ticks, otherwise it is 0
 */
typedef void (*TileDamageFunc)(Arena *arena, int x, int y, Player *firedBy, int damagedealt, int wtype, int level, BOOL bouncingbomb, int emptime, void *clos);

#define I_FAKEDAMAGE "fakedamage-8"
typedef struct Ifakedamage
{
	INTERFACE_HEAD_DECL

	void (*AddFake)(Player *p, FakeDamageFunc damagefunc, void *clos);
	void (*RemoveFake)(Player *p);

	void (*AddArena)(Arena *arena, TileDamageFunc tilefunc, void *clos);
	void (*RemoveArena)(Arena *arena);
	
	int (*GetRepelSpeeds)(Arena *a, int x, int y, int *xspeed, int *yspeed); //pixels, speeds passed by reference, true if affected
	int (*GetGravitySpeeds)(Arena *a, int ship, int x, int y, int *dxthr, int *dythr); //pixels, thrusts passed by reference, true if affected

} Ifakedamage;

#endif

