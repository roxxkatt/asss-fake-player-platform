#ifndef __FAKEWEAPONS_H
#define __FAKEWEAPONS_H

#include "linuxdeps.h"

/*
typedef enum
{
//	STATE_WEAPONFIRECOST	=1,
//	STATE_FIREBACKWARDS		=1,
} fw_state;
*/

#define I_FAKEWEAPONS "fakeweapons-1"
typedef struct Ifakeweapons
{
	INTERFACE_HEAD_DECL
	
	void (*AddFake)(Player *p);
	void (*ChangeFake)(Player *p, int ship, int freq, int x, int y, int time);
	void (*RemoveFake)(Player *p);
	
	int (*AddWeapon)(void); //returns weapon id
	void (*ChangeWeapon)(int id, int weapontype, int level, int shrap, int shraplevel, BOOL bounceshrap, BOOL alt, int range, int firedelay, BOOL killfakes);
	void (*RemoveWeapon)(int id);
	void (*AssignWeapon)(Player *p, int id, BOOL use); //false=stop using
	void (*RemoveWeapons)(Player *p);
	
	//void (*SetState)(Player *p, fw_state state, BOOL set)
	//void (*GreenState)(Player *p, int green, BOOL state); //ex: fw->GreenState(p,PRIZE_FULLCHARGE,TRUE);
	//void (*DropFlag)(Player *p); //flag stuff
	//void (*FireBall)(Player *p); //powerball stuff
	
	//void (*AimAtTarget)(int weapon, int x, int y); //pixels
	//void (*AimAtPlayer)(int weapon, Player *target);

} Ifakeweapons;

#endif
