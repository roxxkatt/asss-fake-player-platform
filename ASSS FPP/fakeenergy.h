#ifndef __FAKEENERGY_H
#define __FAKEENERGY_H

#include "linuxdeps.h"

typedef enum
{
	STATE_TAKEDAMAGE		=1,
	STATE_IGNOREEMP			=2,
	STATE_IGNORESELFDAMAGE	=4,
	STATE_IGNORETEAMDAMAGE	=8,
	STATE_IGNORESAFETY		=16,
} fe_state;

#define I_FAKEENERGY "fakeenergy-1"
typedef struct Ifakeenergy
{
	INTERFACE_HEAD_DECL
	
	void (*AddFake)(Player *p);
	void (*ChangeFake)(Player *p, int energy, int recharge, int spawnx, int spawny, int spawnr);
	void (*RemoveFake)(Player *p);
	
	void (*SetState)(Player *p, fe_state state, BOOL set);
	void (*ModifyEnergy)(Player *p, int newenergy);

} Ifakeenergy;

#endif
