#ifndef __FAKEMOVE_H
#define __FAKEMOVE_H

#include "linuxdeps.h"

#define CB_TARGETREACHED "targetreached-1"
typedef void (*TargetReachedFunc)(Player *p, int x, int y); //tiles

typedef enum
{
	STATE_IGNOREWALLS	=1,
	STATE_STOPATTARGET	=2,
	STATE_STOPNOTARGET	=4,
//	STATE_DECELERATE	=8,
	STATE_IGNOREREPELS	=8,
	STATE_IGNOREGRAVITY	=16,
} fm_state;

#define I_FAKEMOVE "fakemove-1"
typedef struct Ifakemove
{
	INTERFACE_HEAD_DECL
	
	void (*AddFake)(Player *p);
	void (*ChangeFake)(Player *p, int speed, int thrust);
	void (*RemoveFake)(Player *p);
	
	void (*SetState)(Player *p, fm_state state, BOOL set);
	void (*MirrorPlayer)(Player *p, Player *target);
	void (*SetMoveTarget)(Player *p, int x, int y); //tiles, zero to clear
	//void (*MoveAwayFromTarget)(Player *p, int x, int y);
	//void (*LockInBox)(Player *p, int x, int y, int r); //maybe not useful

} Ifakemove;

#endif
