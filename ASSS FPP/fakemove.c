//Fake Movement Module
//By Cheese

/*
TODO:
repels
gravity
	velocity per tick (thrust) is 1000*Gravity/(Distance^2). dist in pixels.
	GravityTopSpeed will be added to a player's top speed as long as at least one Wormhole is applying a thrust of 1 or more.
	The tile radius of the top speed effect is tiles=1.976*(g^.5). 
bricks
doors (+random - needs knowledge of Continuum's security)
bouncefactor
	SpeedAfter=SpeedBefore*(16/BounceFactor)
	
REVISION HISTORY
Aug 30 2010 Cheese	Initial module creation
Jul 30 2012 Cheese	Source code released to public
Dec 11 2012 Cheese	Split FindFake from AddFake
						Added repels
						Added gravity
*/

#include <math.h>

#include "asss.h"
#include "fakemove.h"
#include "fakedamage.h"

local Imodman *mm;
local Ilogman *lm;
local Icmdman *cmd;
local Ichat *chat;
local Iplayerdata *pd;
local Imapdata *map;
local Ifakedamage *dmg;

local LinkedList turrets=LL_INITIALIZER;
local pthread_mutex_t move_mtx=PTHREAD_MUTEX_INITIALIZER;

#define IS_PLAYER(p) ((p)->type == T_CONT)
#define IS_FAKE(p) ((p)->type == T_FAKE)
#define FOR_EACH_LINK(x,y,z) Link *(z); for((z)=LLGetHead(&(x)); (z) && (((y)=(z)->data, (z)=(z)->next) || 1); )
#define IS_TILE_SOLID(tile) ((tile >= 1 && tile <= 161) || (tile >= 192 && tile <= 240) || (tile >= 243 && tile <= 249))
#define MOVE_LOCK() pthread_mutex_lock(&move_mtx);
#define MOVE_UNLOCK() pthread_mutex_unlock(&move_mtx);

typedef struct MotionData
{
	Player *p;
	struct C2SPosition pos;
	int state;
	Player *turret;
	int spd;
	int thr;
	//int rot;
	int tx;
	int ty;
	ticks_t lastmove;
} MotionData;

local Player* fuzzyfind(Arena *a, char* name)
{
	if(!a || !name) return NULL;
	Player *pp=NULL;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(pp,a)
	{
		if(!stricmp(pp->name,name)) goto found;
	}
	FOR_EACH_PLAYER_IN_ARENA(pp,a)
	{
		int len=strlen(name);
		if(len > strlen(pp->name)) continue;
		if(!strnicmp(pp->name,name,len)) goto found;
	}
	pd->Unlock();
	return NULL;
	
found:
	pd->Unlock();
	return pp;
}

local void clearmove(MotionData *md)
{
	LLRemove(&turrets,md);
	afree(md);
}

local MotionData* findfake(Player *p)
{
	if(!p) return NULL;
	if(!IS_FAKE(p)) return NULL;
	
	MotionData *md=NULL;
	// MOVE_LOCK();
	MotionData *fmd;
	FOR_EACH_LINK(turrets,fmd,l)
	{
		if(fmd->p == p) md=fmd;
	}
	// MOVE_UNLOCK();
	
	return md;
}

local void AddFake(Player *p)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	MotionData *md=findfake(p);
	if(!md)
	{
		md=amalloc(sizeof(MotionData));
		
		md->p=p;
		md->state=0;
		md->turret=NULL;
		md->spd=0;
		md->thr=0;
		//md->rot=0;
		md->tx=0;
		md->ty=0;
		md->lastmove=current_ticks();
		
		MOVE_LOCK();
		LLAdd(&turrets,md);
		MOVE_UNLOCK();
	}
}

local void ChangeFake(Player *p, int speed, int thrust)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	MotionData *md=findfake(p);
	if(!md) return;
	
	if(speed >= 0) md->spd=speed;
	if(thrust >= 0) md->thr=thrust;
}

local void RemoveFake(Player *p)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	MOVE_LOCK();
	MotionData *md;
	FOR_EACH_LINK(turrets,md,l)
	{
		if(md->p == p) clearmove(md);
	}
	MOVE_UNLOCK();
}

local void SetState(Player *p, fm_state state, BOOL set)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	MotionData *md=findfake(p);
	if(!md) return;
	
	if(set) md->state |= state;
	else md->state &= ~state;
}

local void MirrorPlayer(Player *p, Player *target)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	if(!target) return;
	
	MotionData *md=findfake(p);
	if(!md) return;
	
	md->turret=target;
}

local void SetMoveTarget(Player *p, int x, int y)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	if((x < 0) || (x > 1024) || (y < 0) || (y > 1024)) return;
	
	MotionData *md=findfake(p);
	if(!md) return;
	
	md->tx=x;
	md->ty=y;
}

local int brickissolid(int x, int y)
{

	return 0;
}

local void mlfunc()
{
	MOVE_LOCK();
	MotionData *md;
	FOR_EACH_LINK(turrets,md,l)
	{
		ticks_t now=current_ticks();
		ticks_t dt=TICK_DIFF(now,md->lastmove);
		
		if(!md->p || !md->p->arena)
		{
			clearmove(md);
			continue;
		}
		
		md->pos.x=md->p->position.x;
		md->pos.y=md->p->position.y;
		md->pos.xspeed=md->p->position.xspeed;
		md->pos.yspeed=md->p->position.yspeed;
		md->pos.rotation=md->p->position.rotation;
		
		if(md->p->flags.is_dead)
		{
			md->pos.x=-1;
			md->pos.y=-1;
			md->pos.xspeed=0;
			md->pos.yspeed=0;
		}
		else if(dt >= 10)
		{
			if(md->turret && (md->turret->arena != md->p->arena)) md->turret=NULL;
			if(md->turret)
			{
				md->pos.x=md->turret->position.x;
				md->pos.y=md->turret->position.y;
				md->pos.xspeed=md->turret->position.xspeed;
				md->pos.yspeed=md->turret->position.yspeed;
				md->pos.rotation=md->turret->position.rotation;
			}
			else
			{
				if(md->tx && md->ty)
				{
					int dx=md->tx*16-md->pos.x;
					int dy=md->ty*16-md->pos.y;
					
					double thrscaling, velscaling, check;
					double angle=atan2(dy,dx);
					int ithrx=md->spd*cos(angle)-md->pos.xspeed;
					int ithry=md->spd*sin(angle)-md->pos.yspeed;
					check=sqrt(pow(ithrx,2)+pow(ithry,2));
					if(check != 0) thrscaling=md->thr/check;
					int nvelx=md->pos.xspeed+thrscaling*ithrx;
					int nvely=md->pos.yspeed+thrscaling*ithry;
					md->pos.xspeed=nvelx;
					md->pos.yspeed=nvely;
					
					if((pow(nvelx,2)+pow(nvely,2)) > pow(md->spd,2))
					{
						check=sqrt(pow(nvelx,2)+pow(nvely,2));
						if(check != 0) velscaling=md->spd/check;
						md->pos.xspeed=velscaling*nvelx;
						md->pos.yspeed=velscaling*nvely;
					}
				}
				
				if(dmg)
				{
				/*
					int gdx=0;
					int gdy=0;
					dmg->GetGravitySpeeds(md->p->arena,md->p->pkt.ship,md->pos.x,md->pos.y,&gdx,&gdy);
					
// GravityTopSpeed will be added to a player's top speed as long as at least one Wormhole is applying a thrust of 1 or more.
// The tile radius of the top speed effect is tiles=1.976*(g^.5).
					md->pos.xspeed+=gdx;
					md->pos.yspeed+=gdy;
					
					double velscaling, check;
					int maxspd=md->spd;
					if((pow(md->pos.xspeed,2)+pow(md->pos.yspeed,2)) > pow(maxspd,2))
					{
						check=sqrt(pow(md->pos.xspeed,2)+pow(md->pos.yspeed,2));
						if(check != 0) velscaling=maxspd/check;
						md->pos.xspeed*=velscaling;
						md->pos.yspeed*=velscaling;
					}
					*/
					/* //crash in here somewhere
					int rxspeed=0;
					int ryspeed=0;
					if(dmg->GetRepelSpeeds(md->p->arena,md->pos.x,md->pos.y,&rxspeed,&ryspeed))
					{
						md->pos.xspeed=rxspeed;
						md->pos.yspeed=ryspeed;
					}
					*/
				}
				
				
				if(!(md->state & STATE_IGNOREWALLS))
				{
					int x=md->pos.x;
					int y=md->pos.y;
					int xspeed=md->pos.xspeed;
					int yspeed=md->pos.yspeed;
					
					int ny=y+yspeed/100;
					int tx=x/16;
					int ty=ny/16;
					
					BOOL blocked=FALSE;
					int tilesy=y/16-ty;
					int tries=1024;
					while(tries)
					{
						if(IS_TILE_SOLID(map->GetTile(md->p->arena,tx,y/16-tilesy)) || brickissolid(tx,ty))
						{
							blocked=TRUE;
							break;
						}
						if(tilesy > 0) tilesy--;
						else tilesy++;
						if(tilesy == 0) break;
						tries--;
					}
					
					if(blocked)
					{
						if(yspeed >= 0) y=ty*16-1;
						else y=(ty+1)*16;
						yspeed=-yspeed;
					}
					else y=ny;
					
					int nx=x+xspeed/100;
					tx=nx/16;
					ty=y/16;
					
					blocked=FALSE;
					int tilesx=x/16-tx;
					tries=1024;
					while(tries)
					{
						if(IS_TILE_SOLID(map->GetTile(md->p->arena,x/16-tilesx,ty)) || brickissolid(tx,ty))
						{
							blocked=TRUE;
							break;
						}
						if(tilesx > 0) tilesx--;
						else tilesx++;
						if(tilesx == 0) break;
						tries--;
					}
					
					if(blocked)
					{
						if(xspeed >= 0) x=tx*16-1;
						else x=(tx+1)*16;
						xspeed=-xspeed;
					}
					else x=nx;
					
					md->pos.x=x;
					md->pos.y=y;
					md->pos.xspeed=xspeed;
					md->pos.yspeed=yspeed;
				}
				else
				{
					md->pos.x+=md->pos.xspeed/100;
					md->pos.y+=md->pos.yspeed/100;
				}
				
				if(md->tx && md->ty)
				{
					if((md->pos.x/16+2 > md->tx) && (md->pos.x/16-2 < md->tx) && (md->pos.y/16+2 > md->ty) && (md->pos.y/16-2 < md->ty))
					{
						Player *tp=md->p; //save copy
						MOVE_UNLOCK();
						DO_CBS(CB_TARGETREACHED,md->p->arena,TargetReachedFunc,(md->p,md->tx,md->ty));
						MOVE_LOCK();
						MotionData *md=findfake(tp); //renew md
						if(!md) continue; //it got deleted, bail out

						if(md->state & STATE_STOPATTARGET)
						{
							md->tx=0;
							md->ty=0;
						}
					}
				}
					
				if(md->state & STATE_STOPNOTARGET)
				{
					if(!md->tx && !md->ty)
					{
						md->pos.xspeed=0;
						md->pos.yspeed=0;
					}
				}
			}
			
			md->lastmove=now;
		}
		
		md->p->position.x=md->pos.x;
		md->p->position.y=md->pos.y;
		md->p->position.xspeed=md->pos.xspeed;
		md->p->position.yspeed=md->pos.yspeed;
		md->p->position.rotation=md->pos.rotation;
	}
	MOVE_UNLOCK();
}

local void NewPlayer(Player *p, int isnew)
{
	if(isnew) return;
	if(IS_FAKE(p)) RemoveFake(p);
	
	MOVE_LOCK();
	MotionData *md;
	FOR_EACH_LINK(turrets,md,l)
	{
		if(md->turret == p) md->turret=NULL;
	}
	MOVE_UNLOCK();
}

local helptext_t cmd_fakemove=
"Targets: Fake player\n"
"Move args:\n"
"[spd=#] Speed\n"
"[thr=#] Thrust\n"
//"[rot=#] Rotation\n"
"[x=#] Move to X\n"
"[y=#] Move to Y\n"
"[p=name] Player to mirror\n"
"[-w] Walls\n"
"Makes fake players move.\n";

local void fakemove(const char *command, const char *params, Player *p, const Target *t)
{
	if(t->type != T_PLAYER) chat->SendMessage(p,"Invalid target.");
	else
	{
		if(t->u.p->type != T_FAKE) chat->SendMessage(p,"Only fakes can be turrets.");
		else
		{
			MotionData *md=findfake(t->u.p);
			if(!md)
			{
				AddFake(t->u.p);
				md=findfake(t->u.p);
				if(!md) return;
			}
			
			char buf[255];
			char *word=NULL;
			const char *tmp=NULL;
			while(strsplit(params," ,:",buf,sizeof(buf),&tmp))
			{
				word=buf;
				if(!strnicmp(word,"spd=",4))
				{
					word+=4;
					int check=atoi(word);
					if(check) md->spd=check;
				}
				else if(!strnicmp(word,"thr=",4))
				{
					word+=4;
					int check=atoi(word);
					if(check) md->thr=check;
				}
				/*else if(!strnicmp(word,"rot=",4))
				{
					word+=4;
					int check=atoi(word);
					if(check) md->rot=check;
				}*/
				else if(!strnicmp(word,"x=",2))
				{
					word+=2;
					int check=atoi(word);
					if(check) md->tx=check;
				}
				else if(!strnicmp(word,"y=",2))
				{
					word+=2;
					int check=atoi(word);
					if(check) md->ty=check;
				}
				else if(!strnicmp(word,"p=",2))
				{
					word+=2;
					md->turret=fuzzyfind(p->arena,word);
				}
				else if(!stricmp(word,"-w"))
				{
					int off=TRUE;
					if(md->state & STATE_IGNOREWALLS) off=FALSE;
					SetState(t->u.p,STATE_IGNOREWALLS,off);
				}
				else if(!stricmp(word,"-s"))
				{
					int off=TRUE;
					if(md->state & STATE_STOPATTARGET) off=FALSE;
					SetState(t->u.p,STATE_STOPATTARGET,off);
				}
				else if(!stricmp(word,"-n"))
				{
					int off=TRUE;
					if(md->state & STATE_STOPNOTARGET) off=FALSE;
					SetState(t->u.p,STATE_STOPNOTARGET,off);
				}
				else if(!stricmp(word,"-test"))
				{
					lm->Log(L_ERROR,"target: (%i,%i)",md->tx,md->ty);
				}
			}
			chat->SendMessage(p,"%s modified.",t->u.p->name);
		}
	}
}

local helptext_t cmd_killmove=
"Targets: Fake player\n"
"Args: none\n"
"Stops movement.";

local void killmove(const char *command, const char *params, Player *p, const Target *t)
{
	if(t->type != T_PLAYER) chat->SendMessage(p,"Invalid target.");
	else
	{
		if(t->u.p->type != T_FAKE) chat->SendMessage(p,"Only fakes can be turrets.");
		else
		{
			RemoveFake(t->u.p);
			
			chat->SendMessage(p,"%s is now immobile.",t->u.p->name);
		}
	}
}

local helptext_t cmd_killallmove=
"Targets: none\n"
"Args: [name]\n"
"Stops all movement, or just for [name].";

local void killallmove(const char *command, const char *params, Player *p, const Target *t)
{
	MOVE_LOCK();
	MotionData *md;
	FOR_EACH_LINK(turrets,md,l)
	{
		if(!params || !strnicmp(md->p->name,params,strlen(params))) clearmove(md);
	}
	MOVE_UNLOCK();
	
	if(p) chat->SendMessage(p,"Killed all movement.",t->u.p->name);
}

local Ifakemove moveint=
{
	INTERFACE_HEAD_INIT(I_FAKEMOVE,"fakemove")
	
	AddFake, ChangeFake, RemoveFake,
	SetState, MirrorPlayer, SetMoveTarget,
};

EXPORT const char info_fakemove[]=
".\n"
"Fake Movement Module\n"
"by Cheese\n"
"Makes fake players move around.";

EXPORT int MM_fakemove(int action, Imodman *mm2, Arena *a)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN,ALLARENAS);
		cmd=mm->GetInterface(I_CMDMAN,ALLARENAS);
		chat=mm->GetInterface(I_CHAT,ALLARENAS);
		pd=mm->GetInterface(I_PLAYERDATA,ALLARENAS);
		map=mm->GetInterface(I_MAPDATA,ALLARENAS);
		if(!lm || !cmd || !chat || !pd || !map) return MM_FAIL;
		
		dmg=mm->GetInterface(I_FAKEDAMAGE,ALLARENAS);
		if(!dmg) lm->Log(L_ERROR,"<fakeenergy> Could not find fakedamage, bots will ignore repels and gravity.");
		
		mm->RegInterface(&moveint,ALLARENAS);
		
		mm->RegCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->RegCallback(CB_NEWPLAYER,NewPlayer,ALLARENAS);
		
		cmd->AddCommand("fm",fakemove,ALLARENAS,cmd_fakemove);
		cmd->AddCommand("fkm",killmove,ALLARENAS,cmd_killmove);
		cmd->AddCommand("fkam",killallmove,ALLARENAS,cmd_killallmove);
		
		lm->Log(L_ERROR,"<fake> Fake Movement Module has loaded.");
		
		return MM_OK;
	}
	else if(action == MM_ATTACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake Movement Module has attached to the arena.");
        
		return MM_OK;
	}
	else if(action == MM_DETACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake Movement Module has detached from the arena.");
        
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		lm->Log(L_ERROR,"<fake> Fake Movement Module has unloaded.");
		
		if(mm->UnregInterface(&moveint,ALLARENAS)) return MM_FAIL;
		
		cmd->RemoveCommand("fm",fakemove,ALLARENAS);
		cmd->RemoveCommand("fkm",killmove,ALLARENAS);
		cmd->RemoveCommand("fkam",killallmove,ALLARENAS);
		
		mm->UnregCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->UnregCallback(CB_NEWPLAYER,NewPlayer,ALLARENAS);
		
		killallmove(NULL,NULL,NULL,NULL);
		LLEmpty(&turrets);
		
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(map);
		mm->ReleaseInterface(dmg);
		
		return MM_OK;
	}
	return MM_FAIL;
}
