/*
Fake Damage Module
By Smong + JoWie + Cheese

TODO - in order of importance:
	Repel (+player affected callback)
	Wormhole gravity
		velocity per tick (thrust) is 1000*Gravity/(Distance^2). dist in pixels.
	Shrapnel (+random - needs knowledge of Continuum's security)
	Full support for all special tiles
	Friendly bricks
	Doors - easy, but random doors hard
	Old weapons (lag)
	Clientset overrides
	Player interpolation - http://www.ssforum.net/index.php?showtopic=19720
	Maybe condense move_xxx() functions into move() overlord - too many function calls kills performance

REVISION HISTORY
Apr 15 2005	Smong	(attempt to fix ?addbot def param freq bug)
Jan 16 2006	Smong	Proper cleanup of turret list on unload
Jan 17 2006	Smong	Damage, a bot1 mod.
Jan 28 2006	Smong	Do cb_kill on <= 142
Feb 23 2006	Smong	Increased enter delay from 200 to 400
						Energy is halfway between initial and maximum
						Ship defaults to warbird settings if starting in spec
Jun 16 2006	Smong	Fixed prox to use pid of triggerer.
Jun 17 2006	Smong	Fixed bomb blast radius (-almost pulling hair out-).
Jun 25 2006	Smong	Made max energy get reloaded on ship change.
						Changed energy level to use initial energy.
Jul 25 2006	Smong	Fixed crashing in windows on arena creation (woo).
						Added support for tiles to take damage (direct hits only).
May 16 2007	Smong	Fixed crash when bot respawns in spec.
						Made the kill callback asynchronous so it happens outside
						The mutex, allows add/remove tracking to be called.
						Recoded respawn_timer to use bd as the key, so the timer
						Can be cancelled if the bot is removed in the mean time.
May 18 2007	Smong	Fixed bots not reloading settings when spawning out of spec
Jun 6 2007	Smong	Made the tile damage callback asynchronous so it happens
						Outside the mutex, allows add/remove tracking to be called.
						Moved mutex destroy to AA_POSTDESTROY to avoid crashes.
Jul 4 2007	Smong	Added some support for the special tiles
						Added setting to ignore team damage
						Small tweaks to the map collision routines
Jul 26 2009	JoWie	Fixed EMP bomb and bouncing bomb damage;
						Bouncing bomb would do normal damage after the last bounce;
						Bouncing emp bomb used EBombDamagePercent and not EBombDamagePercent WITH BBombDamagePercent)
						Region damage tracking now uses EBombDamagePercent and BBombDamagePercent
						Added EMP Bomb Shutdown
Jul 27 2009	JoWie	Added additional parameters to TileDamageFunc
						Added FakeDamageFunc
						Added manageenergy settings for fake players
						ticks_t values now use the TICK_DIFF, TICK_MAKE, TICK_GT macros
						moved all cfg->GetInt to AA_CREATE | AA_CONFCHANGED instead of reading it constantly
Feb 4 2010	JoWie	ASSS 1.5.0rc2 support
						Added a check so that dead players can not collide with bombs / bullets
Feb 21 2010	JoWie	Added double barrel support
Apr 24 2010	JoWie	Now uses the CB_PPK callback so that position packets may be rewritten (there is also no more need for damage->FakePosition)
May 24 2010	JoWie	Slight overhaul of existing interface + additions to allow other modules to look up weapon info (but not modify)
Jun 22 2010	JoWie	Fixed a bug with ?insmod and ?rmmod while there are existing arenas
Aug 30 2010 Cheese	Did a complete overhaul of the module
						Cleaned up module style
						Cleaned up these comments
						Removed arena settings
						Removed manageenergy
						Removed forward declarations
						Removed functions that were only used once
						Added respawn delay
						Added thread and link macros
						Module now only informs other modules that damage is taken, so no position packet is needed
Dec 16 2010 Cheese	Did more cleanup
						Replaced regions with arenas for ease of use
Dec 16 2010 Cheese	Even more cleanup
						Moved energy, recharge, and respawning to another module (maintaining abstraction)
						Removed unneeded parts of the interface
						Cleaned up header
						Added prox mines
May 11 2011	Cheese	Added a testing system
						Added bursts
						Added multifire
May 03 2012	Cheese	Added a testing system
						Added repels
						Added gravity
Jul 30 2012 Cheese	Source code released to public
*/

#include <math.h>

#include "asss.h"
#include "fakedamage.h"

local Imodman *mm;
local Ilogman *lm;
local Iconfig *cfg;
local Iplayerdata *pd;
local Iprng *prng;
local Iarenaman *aman;
local Imapdata *map;
local Imainloop *ml;

#include "fake.h"
#include "objects.h"
local Ifake *fake;
local Inet *net;
int DEBUG=FALSE;
Player *tester;
local Iobjects *lvz;
Target tgt;
Arena *aaaa=NULL;

#define PI 3.1415926535897932384626433832795f
#define TICK_GTE(a,b) (TICK_DIFF(a,b) >= 0)
#define FOR_EACH_LINK(x,y,z) Link *(z); for((z)=LLGetHead(&(x)); (z) && (((y)=(z)->data, (z)=(z)->next) || 1); )
#define IS_TILE_SOLID(tile) ((tile >= 1 && tile <= 161) || (tile >= 192 && tile <= 240) || (tile >= 243 && tile <= 249))
#define THREAD_LOCK(x) pthread_mutex_lock(&(x));
#define THREAD_UNLOCK(x) pthread_mutex_unlock(&(x));
#define WEAPON_LOCK(ad) THREAD_LOCK((ad)->wpnmtx)
#define WEAPON_UNLOCK(ad) THREAD_UNLOCK((ad)->wpnmtx)
#define REGION_LOCK(ad) THREAD_LOCK((ad)->rgnmtx)
#define REGION_UNLOCK(ad) THREAD_UNLOCK((ad)->rgnmtx)

local LinkedList turrets=LL_INITIALIZER;
local pthread_mutex_t turret_mtx=PTHREAD_MUTEX_INITIALIZER;
local LinkedList aset=LL_INITIALIZER;
local pthread_mutex_t aset_mtx=PTHREAD_MUTEX_INITIALIZER;

static int sintab[40];
static double sintabd[40];

typedef struct DamageProxData
{
	ticks_t triggered;	//When was the prox bomb triggered?
	i16 dist;			//How close is this prox bomb currently to the player that triggered it? (this will only decrease)
	i16 delay;			//Bomb:BombExplodeDelay
	i16 pid;			//pid of triggerer (-1 if not triggered). It is possible that the player no longer exists.
} DamageProxData;

typedef struct DamageWeapon
{
	Player *p;
	i32 x; //x1000
	i32 y; //x1000
	i16 xspeed;
	i16 yspeed;
	i16 hitx; //When the bomb hits a wall (in tiles)
	i16 hity;
	u8 bounce; //Bounces left
	u8 proxdistance; //Prox distance for this bomb level (in pixels)
	u8 bouncebomb : 1;
	u8 empbomb : 1;
	u8 fuselit : 1; //A ship came within the prox range
	u8 padding : 5;
	u8 removereason; //Only used during removal

	ticks_t startedat;
	ticks_t lastupdate;
	ticks_t alivetime;
	struct DamageWeapon *nextbarrel; //Circular linked list
	struct Weapons wpn;
	struct DamageProxData prox; //Only if wpn.type is W_PROXBOMB or W_THOR, Do not attempt to access otherwise!
} DamageWeapon;

enum
{
	REMOVEWEAPON_HIT = 1,			//Hit a solid tile
	REMOVEWEAPON_HITPLAYER,			//Not very accurate for non fake players
	REMOVEWEAPON_OTHERBARRELHIT,	//Multifire / double barrel and an other barrel hit a player
	REMOVEWEAPON_SHIPFREQCHANGE,	//The firer changed ship
	REMOVEWEAPON_LEAVEARENA,		//The firer left the arena
	REMOVEWEAPON_EXPIRED,			//bulletalivetime, etc
	REMOVEWEAPON_SAFEZONE,
};

/*
#define FOR_EACH_DAMAGEWEAPON(wpn, arena) \
	for ( \
		link = damage->LLGetHeadWeapons(arena); \
		link && ((wpn = (DamageWeapon*) link->data, link = link->next) || 1); )

/ *
	Link *link;
	DamageWeapon *wpn;
	damage->WeaponLock(arena);
	FOR_EACH_DAMAGEWEAPON(wpn, arena)
	{

	}
	damage->WeaponUnlock(arena);
*/

typedef struct BotData
{
	Player *p;
	FakeDamageFunc damagefunc;
	void *clos;
} BotData;

typedef struct async_tile_params
{
	TileDamageFunc tilefunc;
	Arena *a;
	Player *firedBy;
	int x, y, damage, wpntype, level, emptime;
	BOOL bouncingbomb;
	void *clos;
} async_tile_params;

typedef struct async_fakedamage_params
{
	FakeDamageFunc func;
	Player *fake;
	Player *firedBy;
	int dist;
	int damage;
	int wtype;
	int level;
	BOOL bouncing;
	int emptime;
	void *clos;
} async_fakedamage_params;

typedef struct TileData
{
	Arena *a;
	TileDamageFunc tilefunc;
	void *clos;
} TileData;

typedef struct adata
{
	LinkedList wpnset;
	MPQueue wpnset_new;
	MPQueue wpnset_old;
	pthread_mutex_t wpnmtx;

	LinkedList newWeaponCallbacks;
	LinkedList removeWeaponCallbacks;
	LinkedList weaponsUpdatedCallbacks;

	int BulletDamageLevel;
	int BulletDamageUpgrade;
	int BulletAliveTime;
	BOOL BulletExactDamage;
	int BombDamageLevel;
	int EBombDamagePercent;
	int BBombDamagePercent;
	int EBombShutdownTime;
	int BombAliveTime;
	int BombExplodePixels;
	int ProximityDistance;
	int BombExplodeDelay;
	int MineAliveTime;
	int BurstDamageLevel;
	int GravityBombs;
	int RepelSpeed;
	int RepelTime;
	int RepelDistance;

	struct
	{
		int BulletSpeed;
		int MultiFireAngle;
		int BombSpeed;
		int BombBounceCount;
		BOOL EmpBomb;
		int Radius;
		BOOL DoubleBarrel;
		int BurstSpeed;
		int BurstShrapnel;
	} ship[8];

} adata;
local int adkey;

static long lhypot(register long dx, register long dy)
{
	register unsigned long r, dd;
	dd=dx*dx+dy*dy;
	if(dx < 0) dx=-dx;
	if(dy < 0) dy=-dy;
	r=(dx > dy) ? (dx+(dy>>1)) : (dy+(dx>>1));
	if(r == 0) return (long)r;
	r=(dd/r+r)>>1;
	r=(dd/r+r)>>1;
	r=(dd/r+r)>>1;
	return (long)r;
}

local void AddFake(Player *p, FakeDamageFunc damagefunc, void *clos)
{
	Arena *a=p->arena;
	adata *ad=P_ARENA_DATA(a,adkey);
	BotData *bd=amalloc(sizeof(BotData));

	int ship;
	if(p->pkt.ship == SHIP_SPEC) ship=0;
	else ship=p->pkt.ship;

	bd->p=p;

	bd->damagefunc=damagefunc;
	bd->clos=clos;

	THREAD_LOCK(turret_mtx);
	LLAdd(&turrets,bd);
	THREAD_UNLOCK(turret_mtx);
}

local void RemoveFake(Player *p)
{
	THREAD_LOCK(turret_mtx);
	BotData *bd;
	FOR_EACH_LINK(turrets,bd,l)
	{
		if(bd->p == p)
		{
			LLRemove(&turrets,bd);
			afree(bd);
			break;
		}
	}
	THREAD_UNLOCK(turret_mtx);
}

local void remove_weapon(adata *ad, DamageWeapon *dw, BOOL removeAllBarrels, Link **_nextLink, u8 removereason)
{
	DamageWeapon *dw2, *next;
	Link *nextLink=NULL;
	if(_nextLink) nextLink=*_nextLink;

	if(removeAllBarrels)
	{
		dw2=dw->nextbarrel;
		while(dw2 && (dw2 != dw)) //do not clear dw, this is done below
		{
			next=dw2->nextbarrel;
			if(nextLink && nextLink->data == dw2) nextLink=nextLink->next;

			LLRemove(&ad->wpnset,dw2);
			if(removereason == REMOVEWEAPON_HITPLAYER) dw2->removereason=REMOVEWEAPON_OTHERBARRELHIT;
			else dw2->removereason=removereason;
			MPAdd(&ad->wpnset_old,dw2);
			dw2=next;
		}
	}
	else
	{
		if(dw->nextbarrel) //relink
		{
			dw2=dw;
			while(dw2->nextbarrel != dw)
			{
				dw2=dw2->nextbarrel;
				if(!dw2) break;
			}

			//we now have the barrel before this one
			dw2->nextbarrel=dw->nextbarrel;
			if(dw2->nextbarrel == dw2) dw2->nextbarrel=NULL; //last remaining barrel
		}
	}

	if(nextLink && nextLink->data == dw) nextLink=nextLink->next;
	LLRemove(&ad->wpnset,dw);
	dw->removereason=removereason;
	MPAdd(&ad->wpnset_old,dw);

	if(_nextLink) *_nextLink=nextLink;
}

local void remove_weapons(adata *ad, Player *p, u8 removereason)
{
	//DamageWeapon *dw;
	//FOR_EACH_LINK(ad->wpnset,dw,l)
//<ghettocode>
	Link *l, *next;
	for (l=LLGetHead(&ad->wpnset); l; l=next)
	{
		DamageWeapon *dw=l->data;
		next=l->next;
		if(dw->p == p) remove_weapon(ad,dw,TRUE,(next ? &next : NULL),removereason);
//</ghettocode>
#if deprecated
		else if(dw->fuselit && dw->prox.who == p) dw->prox.who=NULL; //cancel prox by distance
#endif
	}
}

local void AddArena(Arena *a, TileDamageFunc tilefunc, void *clos)
{
	if(tilefunc)
	{
		TileData *td=amalloc(sizeof(TileData));

		td->a=a;
		td->tilefunc=tilefunc;
		td->clos=clos;

		THREAD_LOCK(aset_mtx);
		LLAdd(&aset,td);
		THREAD_UNLOCK(aset_mtx);
	}
}

local void RemoveArena(Arena *a)
{
	THREAD_LOCK(aset_mtx);
	TileData *td;
	FOR_EACH_LINK(aset,td,l)
	{
		if(td->a == a)
		{
			LLRemove(&aset,td);
			afree(td);
		}
	}
	THREAD_UNLOCK(aset_mtx);
}

local DamageWeapon* add_weapon(adata *ad, Player *p, const struct C2SPosition *pos)
{
	DamageWeapon *dw;
	int ship=p->pkt.ship;
	int speed;
	int shipradius=ad->ship[ship].Radius;
	int memsize=sizeof(DamageWeapon)-sizeof(DamageProxData);

	if(pos->weapon.type == W_PROXBOMB || pos->weapon.type == W_THOR) dw=amalloc(memsize+sizeof(DamageProxData));
	else dw=amalloc(memsize);

	dw->p=p;
	dw->x=pos->x*1000;
	dw->y=pos->y*1000;
	dw->xspeed=pos->xspeed;
	dw->yspeed=pos->yspeed;
	//lag the weapon on purpose, otherwise a bomb that kills the bot will fly straight through. fixme: recheck when prox is fixed.
	dw->startedat=current_ticks();
	dw->lastupdate=current_ticks();
	//dw->startedat=dw->lastupdate=pos->time; //TODO
	dw->wpn=pos->weapon;
	dw->nextbarrel=NULL;

	if(pos->weapon.type == W_BULLET || pos->weapon.type == W_BOUNCEBULLET)
	{
		speed=ad->ship[ship].BulletSpeed;
		dw->xspeed+=(int)round(speed*sin((pos->rotation/40.0)*2*PI));
		dw->yspeed+=(int)round(speed*sin((((pos->rotation + 30) % 40)/40.0)*2*PI));
		dw->alivetime=TICK_MAKE(ad->BulletAliveTime);
		if(pos->weapon.type == W_BOUNCEBULLET) dw->bounce=1;

		//multifire, +2 barrels. They start from center and move outward.
		//Rotation Points / 1000. One rotation point equals exactly 9�
		if(pos->weapon.alternate) 
		{
			dw->nextbarrel=amalloc(memsize); //barrel 2 or 3
			memcpy(dw->nextbarrel,dw,memsize);
			dw->nextbarrel->nextbarrel=amalloc(memsize); //barrel 3 or 4
			memcpy(dw->nextbarrel->nextbarrel,dw,memsize);
			dw->nextbarrel->nextbarrel->nextbarrel=dw;
			
			int angle=ad->ship[ship].MultiFireAngle;
			//int rot=(pos->rotation+30) % 40;
			int rot=pos->rotation;
			dw->nextbarrel->xspeed=pos->xspeed+(int)round(speed*sin((rot/40.0+angle/40000.0)*2*PI));
			dw->nextbarrel->yspeed=pos->yspeed+(int)round(speed*-cos((rot/40.0+angle/40000.0)*2*PI));
			dw->nextbarrel->nextbarrel->xspeed=pos->xspeed+(int)round(speed*sin((rot/40.0-angle/40000.0)*2*PI));
			dw->nextbarrel->nextbarrel->yspeed=pos->yspeed+(int)round(speed*-cos((rot/40.0-angle/40000.0)*2*PI));
		}
		
		if(ad->ship[ship].DoubleBarrel)
		{
			DamageWeapon *target=dw;
			if(dw->nextbarrel) target=dw->nextbarrel;
			DamageWeapon *newdw=amalloc(memsize); //barrel 2
			memcpy(newdw,dw,memsize);
			dw->nextbarrel=newdw;
			newdw->nextbarrel=target;

			float offset1, offset2;
			offset2=shipradius*0.7;
			if(shipradius == 14) offset1=offset2; //special case, probably for VIE compatability
			else offset1=offset2+1;

			int xchg=(int)round(offset1*sintabd[(pos->rotation+30) % 40]*1000);
			int ychg=(int)round(offset1*sintabd[pos->rotation]*1000);
			dw->x+=xchg;
			dw->y-=ychg;
			dw->nextbarrel->x+=-xchg;
			dw->nextbarrel->y-=-ychg;
		}
		
		if(DEBUG && tester && (p->arena == tester->arena))
		{
			DamageWeapon *dwt=dw;
			int ii=0;
			while(dwt)
			{
				struct Weapons weapon={ W_BOUNCEBULLET,3,0,0,0,0 };
				struct S2CWeapons packet={
					S2C_WEAPON,0,pos->time+ii,
					dwt->x/1000,dwt->yspeed,tester->pid,dwt->xspeed,0,
					STATUS_STEALTH|STATUS_CLOAK|STATUS_UFO,0,dwt->y/1000,0 };
				packet.weapon=weapon;
				net->SendToArena(p->arena,NULL,(byte*)&packet,sizeof(struct S2CWeapons)-sizeof(struct ExtraPosData),NET_RELIABLE);
				
				ii++;
				dwt=dwt->nextbarrel;
				if(dwt == dw) break;
			}
		}
	}
	else if(pos->weapon.type == W_BOMB || pos->weapon.type == W_PROXBOMB || pos->weapon.type == W_THOR)
	{
		if(!pos->weapon.alternate)
		{
			speed=ad->ship[ship].BombSpeed;
			dw->xspeed+=(speed*sintab[pos->rotation]) >> 7;
			dw->yspeed+=(speed*sintab[(pos->rotation + 30) % 40]) >> 7;
			dw->alivetime=TICK_MAKE(ad->BombAliveTime);
			
			if(pos->weapon.type == W_BOMB || pos->weapon.type == W_PROXBOMB)
			{
				dw->bounce=ad->ship[ship].BombBounceCount;
				dw->bouncebomb=!!dw->bounce; // !! = 42 -> 1
				dw->empbomb=ad->ship[ship].EmpBomb;
			}
		}
		else //mine
		{
			dw->xspeed=0;
			dw->yspeed=0;
			dw->alivetime=TICK_MAKE(ad->MineAliveTime);
			dw->empbomb=ad->ship[ship].EmpBomb;
			dw->bouncebomb=0;
		}
		
		if(pos->weapon.type == W_PROXBOMB || pos->weapon.type == W_THOR) //prox
		{
			int proxdistance=ad->ProximityDistance;
			proxdistance+=pos->weapon.level;
			if(pos->weapon.type == W_THOR) proxdistance+=3;

			proxdistance=proxdistance*16;
			CLIP(proxdistance,0,255); //ProximityDistance > 12
			dw->proxdistance=proxdistance;

			dw->prox.delay=ad->BombExplodeDelay;
			dw->prox.pid=-1; //make this unset/no one
		}
	}
	else if(pos->weapon.type == W_REPEL)
	{
		dw->xspeed=0;
		dw->yspeed=0;
		dw->alivetime=TICK_MAKE(ad->RepelTime);
		dw->empbomb=0;
		dw->bouncebomb=0;
		dw->bounce=0;
	}
	//else if(pos->weapon.type == W_DECOY)
	else if(pos->weapon.type == W_BURST)
	{
		speed=ad->ship[ship].BurstSpeed;
		int shrap=ad->ship[ship].BurstShrapnel;
		dw->alivetime=TICK_MAKE(ad->BulletAliveTime);
		dw->bounce=FALSE;
		
		DamageWeapon *firstbarrel=dw;
		DamageWeapon *lastbarrel=dw;
		int i; for(i=0; i<shrap; i++)
		{
			if(i > 0)
			{
				lastbarrel=dw;
				dw=amalloc(memsize);
				memcpy(dw,lastbarrel,memsize);
				lastbarrel->nextbarrel=dw;
				dw->nextbarrel=firstbarrel;
			}
			
			dw->xspeed=(int)round(speed*sin((1.0*i/shrap)*2*PI));
			dw->yspeed=-(int)round(speed*cos((1.0*i/shrap)*2*PI));
		}
		dw=firstbarrel;
		
		if(DEBUG && tester && (p->arena == tester->arena))
		{
			for(i=0; i<shrap; i++)
			{
				int xspeed=(int)round(speed*sin((1.0*i/shrap)*2*PI));
				int yspeed=-(int)round(speed*cos((1.0*i/shrap)*2*PI));
				struct Weapons weapon={ W_BOUNCEBULLET,3,0,0,0,0 };
				struct S2CWeapons packet={
					S2C_WEAPON,0,pos->time+i,
					pos->x,yspeed,tester->pid,xspeed,0,
					STATUS_STEALTH|STATUS_CLOAK|STATUS_UFO,0,pos->y,0 };
				packet.weapon=weapon;
				net->SendToArena(p->arena,NULL,(byte*)&packet,sizeof(struct S2CWeapons)-sizeof(struct ExtraPosData),NET_RELIABLE);
			}
		}
	}
	else //not handled
	{
		afree(dw);
		return NULL;
	}

	DamageWeapon *dwi=dw;
	do
	{
		LLAdd(&ad->wpnset,dwi);
		MPAdd(&ad->wpnset_new,dwi);
		dwi=dwi->nextbarrel;
	} while(dwi && (dwi != dw));
	
	return dw;
}

local int brickissolid(int x, int y)
{

	return 0;
}

local int move_vertical(Arena *a, DamageWeapon *dw)
{
	i32 x=dw->x;
	i32 y=dw->y;
	i16 xspeed=dw->xspeed;
	i16 yspeed=dw->yspeed;
	i16 hitx=dw->hitx;
	i16 hity=dw->hity;

	int bounced=0;
	int ny=y+yspeed;
	int tx=(x/1000) >> 4;
	int ty=(ny/1000) >> 4;
	
	if(IS_TILE_SOLID(map->GetTile(a,tx,ty)) || brickissolid(tx,ty))
	{
		hitx=tx;
		hity=ty;
		if(yspeed >= 0) y=(ty << 4) * 1000 - 1;
		else y=((ty + 1) << 4) * 1000;

		yspeed=-yspeed;
		bounced=1;
	}
	else y=ny;
	
	dw->x=x;
	dw->y=y;
	dw->xspeed=xspeed;
	dw->yspeed=yspeed;
	dw->hitx=hitx;
	dw->hity=hity;

	return bounced;
}

local int move_horizontal(Arena *a, DamageWeapon *dw)
{
	i32 x=dw->x;
	i32 y=dw->y;
	i16 xspeed=dw->xspeed;
	i16 yspeed=dw->yspeed;
	i16 hitx=dw->hitx;
	i16 hity=dw->hity;

	int bounced=0;
	int nx=x+xspeed;
	int tx=(nx/1000) >> 4;
	int ty=(y/1000) >> 4;
	
	if(IS_TILE_SOLID(map->GetTile(a,tx,ty)) || brickissolid(tx,ty))
	{
		hitx=tx;
		hity=ty;
		if(xspeed >= 0) x=(tx << 4) * 1000 - 1;
		else x=((tx + 1) << 4) * 1000;

		xspeed=-xspeed;
		bounced=1;
	}
	else x=nx;

	dw->x=x;
	dw->y=y;
	dw->xspeed=xspeed;
	dw->yspeed=yspeed;
	dw->hitx=hitx;
	dw->hity=hity;

	return bounced;
}

local inline int move_thor(DamageWeapon *dw, ticks_t dt)
{
	dw->x+=dw->xspeed*dt;
	dw->y+=dw->yspeed*dt;
	//note: thors disappear in the center of wormholes + thor eating tiles (probably same)
	return 0;
}

local inline int move_bouncebullet(Arena *a, DamageWeapon *dw, ticks_t dt)
{
	while(dt)
	{
		dt--;
		move_vertical(a,dw);
		move_horizontal(a,dw);
	}
	return 0;
}

local inline int move_normal(Arena *a, DamageWeapon *dw, ticks_t dt)
{
	int die=0;
	while(dt && !die)
	{
		dt--;
		die+=move_vertical(a,dw);
		die+=move_horizontal(a,dw);
	}
	return die;
}

local inline int move_bounce(Arena *a, DamageWeapon *dw, ticks_t dt)
{
	while(dt)
	{
		dt--;
		dw->bounce-=move_vertical(a,dw);
		if(!dw->bounce) return move_normal(a,dw,dt);

		dw->bounce-=move_horizontal(a,dw);
		if(!dw->bounce) return move_normal(a,dw,dt);
	}
	return 0;
}

local inline int move_burst(Arena *a, DamageWeapon *dw, ticks_t dt)
{
	while(dt)
	{
		dt--;
		if(move_vertical(a,dw)) dw->bounce=TRUE;
		if(move_horizontal(a,dw)) dw->bounce=TRUE;
	}
	return 0;
}

local int GetRepelSpeeds(Arena *a, int x, int y, int *xspeed, int *yspeed)
{
/*
	if(!xspeed || !yspeed) return 0; //ignore null pointers
	
	adata *ad=NULL;
	Arena *aa;
	adata *add;
	Link *link;
	aman->Lock();
	FOR_EACH_ARENA_P(aa,add,adkey)
	{
		if(aa != a) continue;
		ad=add;
	}
	aman->Unlock();
	if(!ad) return 0; //bad arena
	
	int repels=0;
	DamageWeapon *dw;
	FOR_EACH_LINK(ad->wpnset,dw,l) //apply all the repels within range, only the last one counts, probably
	{
		if(dw->wpn.type != W_REPEL) continue;
		
		int dx=x-dw->x/1000;
		int dy=y-dw->y/1000;
		int dist=lhypot(dx,dy);
		if(dist > ad->RepelDistance) continue;
		
		//angle=atan(dy/dx)=atan((repy-bomby)/(repx-bombx))
		//xspeed=repelspeed*cos(angle)=repelspeed/sqrt(dx^2/dy^2+1)
		//yspeed=repelspeed*sin(angle)=repelspeed/sqrt(dx^2/dy^2+1)*(dx/dy)
		double temp=1.0/sqrt(1.0*dx*dx/(1.0*dy*dy+1));
		*xspeed=(int)(1.0*ad->RepelSpeed*temp);
		if(!dy) dy++; //no div 0
		*yspeed=(int)(1.0*ad->RepelSpeed*temp*(dx/dy));
		
		repels++;
lm->Log(L_ERROR,"fd rep x=%i y=%i dist=%i maxdist=%i xs=%i ys=%i",x,y,dist,ad->RepelDistance,*xspeed,*yspeed);
	}

	return repels;
	*/
	return 0;
}

local int GetGravitySpeeds(Arena *a, int ship, int x, int y, int *xspeed, int *yspeed)
{
/*
	if(!xspeed || !yspeed) return 0; //ignore null pointers
	
	adata *ad=NULL;
	Arena *aa;
	adata *add;
	Link *link;
	aman->Lock();
	FOR_EACH_ARENA_P(aa,add,adkey)
	{
		if(aa != a) continue;
		ad=add;
	}
	aman->Unlock();
	if(!ad) return 0; //bad arena
	*/
	//velocity per tick (thrust) is 1000*Gravity/(Distance^2). dist in pixels.
	
	
	//probably iterate over the center tile of every wormhole in arena and sum effects
	
		/*
	int dx=x-md->pos.x;
	int dy=y-md->pos.y;
	
	double thrscaling, velscaling, check;
	double angle=atan2(dy,dx);
	int ithrx=md->spd*cos(angle)-md->pos.xspeed;
	int ithry=md->spd*sin(angle)-md->pos.yspeed;
	check=sqrt(pow(ithrx,2)+pow(ithry,2));
	if(check != 0) thrscaling=md->thr/check;
	int nvelx=md->pos.xspeed+thrscaling*ithrx;
	int nvely=md->pos.yspeed+thrscaling*ithry;
	md->pos.xspeed=nvelx;
	md->pos.yspeed=nvely;
	*/
	return 0;
}

/*
local inline int prox_collision(int wx, int wy, int wr, Player *p)
{
	if(p->position.x / 16 < (wx - wr) / 16) return FALSE;
	if(p->position.x / 16 > (wx + wr) / 16) return FALSE;
	if(p->position.y / 16 < (wy - wr) / 16) return FALSE;
	if(p->position.y / 16 > (wy + wr) / 16) return FALSE;

	//circular collision
	if(lhypot(p->position.x / 16 - wx / 16, p->position.y / 16 - wy / 16) > wr / 16) return FALSE;

	return TRUE;
}
*/

local int async_tile_timer(void *vp)
{
	async_tile_params *d=vp;
	d->tilefunc(d->a,d->x,d->y,d->firedBy,d->damage,d->wpntype,d->level,d->bouncingbomb,d->emptime,d->clos);
	afree(d);
	return 0;
}

local void do_tile_damage(Arena *a, DamageWeapon *dw)
{
	adata *ad=P_ARENA_DATA(a,adkey);
	
	int x=dw->x / 1000;
	int y=dw->y / 1000;

	int shutdownTime=0;
	int wtype=dw->wpn.type;
	u32 damage=0;

	if(wtype == W_BULLET)
	{
		u32 maxdamage=ad->BulletDamageLevel + ad->BulletDamageUpgrade * dw->wpn.level;
		if(maxdamage < 1) return;

		if(ad->BulletExactDamage) damage=maxdamage;
		else damage=prng->Rand() % maxdamage;
		
if(aaaa)
{
lvz->Move(&tgt,14000,x,y,0,0);
lvz->Toggle(&tgt,14000,1);
}
	}
	else if(wtype == W_BOMB || wtype == W_PROXBOMB)
	{
		damage=ad->BombDamageLevel;

		if(dw->empbomb) damage=damage * ad->EBombDamagePercent / 1000;
		if(dw->bouncebomb) damage=damage * ad->BBombDamagePercent / 1000;
		if(dw->empbomb && damage) shutdownTime=ad->EBombShutdownTime;
	}

	THREAD_LOCK(aset_mtx);
	TileData *td;
	FOR_EACH_LINK(aset,td,l)
	{
		if(damage && td->tilefunc && (td->a == a))
		{
			async_tile_params *d=amalloc(sizeof(async_tile_params));
			d->a=a;
			d->x=x;
			d->y=y;
			d->firedBy=dw->p;
			d->damage=damage;
			d->wpntype=dw->wpn.type;
			d->level=dw->wpn.level;
			d->emptime=shutdownTime;
			d->bouncingbomb=!!dw->bouncebomb;
			d->tilefunc=td->tilefunc;
			d->clos=td->clos;

			ml->SetTimer(async_tile_timer,0,0,d,NULL);
		}
	}
	THREAD_UNLOCK(aset_mtx);
}

local int async_fakedamage_timer(void *vp)
{
	async_fakedamage_params *d=vp;
	d->func(d->fake,d->firedBy,d->dist,d->damage,d->wtype,d->level,d->bouncing,d->emptime,d->clos);
	afree(d);
	return 0;
}

local void do_bullet_damage(Arena *a, DamageWeapon *dw, Player *p)
{
	adata *ad=P_ARENA_DATA(a,adkey);
	u32 damage, maxdamage;
	ticks_t now=current_ticks();
	
	if(dw->wpn.type == W_BURST) maxdamage=ad->BurstDamageLevel;
	else maxdamage=ad->BulletDamageLevel + ad->BulletDamageUpgrade * dw->wpn.level;

	if(maxdamage < 1) return;

	if(ad->BulletExactDamage) damage=maxdamage;
	else damage=prng->Rand() % maxdamage;

	THREAD_LOCK(turret_mtx);
	BotData *bd;
	FOR_EACH_LINK(turrets,bd,l)
	{
		if(bd->p == p)
		{
			if(bd->damagefunc)
			{
				async_fakedamage_params *params=amalloc(sizeof(async_fakedamage_params));
				params->func=bd->damagefunc;
				params->fake=bd->p;
				params->firedBy=dw->p;
				params->dist=0;
				params->damage=damage;
				params->wtype=dw->wpn.type;
				params->level=dw->wpn.level;
				params->bouncing=((dw->wpn.type == W_BOUNCEBULLET) || (dw->wpn.type == W_BURST));
				params->emptime=0;
				params->clos=bd->clos;
				ml->SetTimer(async_fakedamage_timer,0,0,params,NULL);
			}
			break;
		}
	}
	THREAD_UNLOCK(turret_mtx);
}

local void do_splash_damage(Arena *a, DamageWeapon *dw)
{
	adata *ad=P_ARENA_DATA(a,adkey);
	int x=dw->x / 1000;
	int y=dw->y / 1000;
	
	int radius=ad->BombExplodePixels;
	u32 damage, maxdamage=ad->BombDamageLevel;
	ticks_t now=current_ticks();

	radius=radius * (dw->wpn.level + (dw->wpn.type == W_THOR ? 4 : 1)); //adjust radius according to bomb level (thor being L4)
	if(radius < 1 || maxdamage < 1) return; //avoid divide by zero error and wasting time

	if(dw->empbomb) maxdamage=maxdamage * ad->EBombDamagePercent / 1000;
	if(dw->bouncebomb) maxdamage=maxdamage * ad->BBombDamagePercent / 1000;

	THREAD_LOCK(turret_mtx);
	BotData *bd;
	FOR_EACH_LINK(turrets,bd,l)
	{
		if(bd->p->arena == a && bd->p->pkt.ship != SHIP_SPEC)
		{
#if adjust1
			int bx=bd->p->position.x + (bd->p->position.xspeed * (now - bd->p->position.time) / 1000);
			int by=bd->p->position.y + (bd->p->position.yspeed * (now - bd->p->position.time) / 1000);
#else
			int bx=bd->p->position.x;
			int by=bd->p->position.y;
#endif
			int shutdownTime=0;

			int dist=lhypot(x - bx, y - by);
			if(dist < radius)
			{
				//damage=maxdamage * (radius - dist) / radius;
				damage=dist * - (i32)maxdamage / radius + maxdamage;
				if(dw->empbomb && maxdamage) //continuum does not do emp if bomb damage is 0
				{
					int EBombShutDownTime=ad->EBombShutdownTime;
					shutdownTime=dist * -EBombShutDownTime / radius + EBombShutDownTime;

					//if(TICK_GT(now + shutdownTime, bd->empshutdown_expiresat)) bd->empshutdown_expiresat=TICK_MAKE(now + shutdownTime);
				}

				if(bd->damagefunc)
				{
					async_fakedamage_params *params=amalloc(sizeof(async_fakedamage_params));
					params->func=bd->damagefunc;
					params->fake=bd->p;
					params->firedBy=dw->p;
					params->dist=dist;
					params->damage=damage;
					params->wtype=dw->wpn.type;
					params->level=dw->wpn.level;
					params->bouncing=!!dw->bouncebomb;
					params->emptime=shutdownTime;
					params->clos=bd->clos;
					ml->SetTimer(async_fakedamage_timer,0,0,params,NULL);
				}
			}
		}
	}
	THREAD_UNLOCK(turret_mtx);
}

local void mlfunc()
{
	Link *link;
	Arena *a;
	adata *ad;
	ticks_t now=current_ticks();
	DamageWeapon *wpn;

	aman->Lock();
	FOR_EACH_ARENA_P(a,ad,adkey)
	{
		if(a->status != ARENA_RUNNING) continue;

		WEAPON_LOCK(ad);
		//DamageWeapon *dw;
		//FOR_EACH_LINK(ad->wpnset,dw,l)
//<ghettocode>
		Link *l, *next;
		for(l=LLGetHead(&ad->wpnset); l; l=next)
		{
			DamageWeapon *dw=l->data;
			next=l->next;
			if(TICK_GTE(TICK_DIFF(now,dw->startedat),dw->alivetime)) remove_weapon(ad,dw,TRUE,(next ? &next : NULL),REMOVEWEAPON_EXPIRED);
//</ghettocode>
			else if(TICK_DIFF(now,dw->lastupdate) > 0)
			{
				ticks_t dt=TICK_DIFF(now,dw->lastupdate);
				u8 remove=0;
				BOOL removeAllBarrels=FALSE;

				if(dw->wpn.type == W_BOUNCEBULLET) move_bouncebullet(a,dw,dt);
				else if(dw->wpn.type == W_BULLET)
				{
					int xspeed=0;
					int yspeed=0;
					if(GetRepelSpeeds(a,dw->x/1000,dw->y/1000,&xspeed,&yspeed))
					{
						dw->xspeed=xspeed;
						dw->yspeed=yspeed;
					}
					if(GetGravitySpeeds(a,1,dw->x/1000,dw->y/1000,&xspeed,&yspeed))
					{
						dw->xspeed+=xspeed;
						dw->yspeed+=yspeed;
					}
					if(move_normal(a,dw,dt)) remove=REMOVEWEAPON_HIT;
					if(remove) do_tile_damage(a,dw);
				}
				else if(dw->wpn.type == W_BOMB || dw->wpn.type == W_PROXBOMB)
				{
					if(dw->bounce)
					{
						if(move_bounce(a,dw,dt)) remove=REMOVEWEAPON_HIT;
					}
					else
					{
						if(move_normal(a,dw,dt)) remove=REMOVEWEAPON_HIT;
					}

					if(remove)
					{
						do_splash_damage(a,dw);
						do_tile_damage(a,dw);
					}
				}
				else if(dw->wpn.type == W_THOR) move_thor(dw,dt);
				//else if(dw->wpn.type == W_REPEL)
				//else if(dw->wpn.type == W_DECOY)
				else if(dw->wpn.type == W_BURST) move_burst(a,dw,dt);

				if(dw->fuselit)
				{
					//check distance between the bomb and the player that triggered it, also check to see if the fuse has run out.
					Player *p=pd->PidToPlayer(dw->prox.pid);
					int dist;
					if(p) dist=lhypot(p->position.x - dw->x/1000, p->position.y - dw->y/1000);
					else dist=10000; //triggerer left arena, set dist really large to force it to explode.

					//if the distance increases or the fuse burns out then explode. note: if the distance is 0 it can only increase
					if(!dist || (dist > dw->prox.dist) || (TICK_DIFF(now, dw->prox.triggered) >= dw->prox.delay))
					{
						do_splash_damage(a,dw);
						dw->alivetime=0;
						remove=REMOVEWEAPON_HITPLAYER;
						removeAllBarrels=TRUE;
					}
					else
					{
						dw->prox.dist=dist;
						dw->lastupdate=now;
					}
				}

				if(!remove && !dw->fuselit && ((dw->wpn.type != W_BURST) || ((dw->wpn.type == W_BURST) && dw->bounce)))
				{
					//check for collisions with enemy ships
					Link *link;
					Player *p;
					int wx=dw->x/1000;
					int wy=dw->y/1000;
					int wr;

					//something funny with prox radius, maybe just continuum messing up.
					if(dw->wpn.type == W_PROXBOMB || dw->wpn.type == W_THOR) wr=dw->proxdistance;
					else if(dw->wpn.type == W_BOMB) wr=7;
					else wr=3; //bullet radius!

					pd->Lock();
					FOR_EACH_PLAYER(p)
					{
						if(p && (p->arena == a) && (p->pkt.ship != SHIP_SPEC) && 
						(dw && dw->p && (p->pkt.freq != dw->p->pkt.freq)) && !p->flags.is_dead)
						{	
							adata *ad=P_ARENA_DATA(p->arena,adkey);
							int radius=0;
							
							if(wr <= 8)
							{
								radius=ad->ship[p->pkt.ship].Radius;
								if(!radius) radius=14;
							}
//adjust for 5 ticks, 5=half spd (usually)
#if adjust2
#define XADJUST (p->position.xspeed * 5 / 1000)
#define YADJUST (p->position.yspeed * 5 / 1000)
#else
#define XADJUST (0)
#define YADJUST (0)
#endif
							BOOL collision=TRUE;
							if( (p->position.x+XADJUST+radius < wx-wr)
							|| (p->position.x+XADJUST-radius > wx+wr)
							|| (p->position.y+YADJUST+radius < wy-wr)
							|| (p->position.y+YADJUST-radius > wy+wr) ) collision=FALSE;
							//prox uses circular collision
							//if(wr > 8) if(lhypot(p->position.x - wx, p->position.y - wy) > wr) return FALSE;

							if(collision)
							{
								if(dw->wpn.type == W_PROXBOMB || dw->wpn.type == W_THOR)
								{
									dw->fuselit=1;
									dw->prox.triggered=now;
									dw->prox.pid=p->pid;
									//dw->prox.x=p->position.x;
									//dw->prox.y=p->position.y;
									dw->prox.dist=lhypot(p->position.x - wx, p->position.y - wy);
									//reduce resolution to 1 tile
									//dw->prox.dist=(dw->prox.dist / 16) * 16;
									break;
								}
								else
								{
									if(dw->wpn.type == W_BOMB) do_splash_damage(a,dw);
									else if(p->type == T_FAKE) do_bullet_damage(a,dw,p); //not a bomb, do bullet damage
									dw->alivetime=0;
									remove=REMOVEWEAPON_HITPLAYER;
									if(dw->wpn.type != W_BURST) removeAllBarrels=TRUE;
									break;
								}
							}
						}
					}
					pd->Unlock();
				}
				dw->lastupdate=now;

				if(remove) remove_weapon(ad,dw,removeAllBarrels,(next ? &next : NULL),remove);
			}
		}
		WEAPON_UNLOCK(ad);
	}
	aman->Unlock();
}

local void ShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	adata *ad=P_ARENA_DATA(p->arena,adkey);

	WEAPON_LOCK(ad);
	remove_weapons(ad,p,REMOVEWEAPON_SHIPFREQCHANGE);
	WEAPON_UNLOCK(ad);
}

local void PlayerAction(Player *p, int action, Arena *a)
{
	if(action == PA_LEAVEARENA)
	{
		adata *ad=P_ARENA_DATA(a,adkey);

		WEAPON_LOCK(ad);
		remove_weapons(ad,p,REMOVEWEAPON_LEAVEARENA);
		WEAPON_UNLOCK(ad);
	}
}

local void ArenaAction(Arena *a, int action)
{
	adata *ad=P_ARENA_DATA(a,adkey);
	struct WeaponData *dw;

	if(action == AA_PRECREATE)
	{
		LLInit(&ad->wpnset);
		MPInit(&ad->wpnset_new);
		MPInit(&ad->wpnset_old);
		pthread_mutex_init(&ad->wpnmtx,NULL);

		LLInit(&ad->newWeaponCallbacks);
		LLInit(&ad->removeWeaponCallbacks);
		LLInit(&ad->weaponsUpdatedCallbacks);
	}
	else if(action == AA_DESTROY)
	{
		LLEmpty(&ad->newWeaponCallbacks);
		LLEmpty(&ad->removeWeaponCallbacks);
		LLEmpty(&ad->weaponsUpdatedCallbacks);

		THREAD_LOCK(turret_mtx);
		BotData *bd;
		FOR_EACH_LINK(turrets,bd,l)
		{
			if(bd->p->arena == a)
			{
				LLRemove(&turrets,bd);
				afree(bd);
				break;
			}
		}
		THREAD_UNLOCK(turret_mtx);

		WEAPON_LOCK(ad);
		LLEnum(&ad->wpnset,afree);
		LLEmpty(&ad->wpnset);
		MPDestroy(&ad->wpnset_new);
		while((dw=MPTryRemove(&ad->wpnset_old))) afree(dw);
		MPDestroy(&ad->wpnset_old);
		WEAPON_UNLOCK(ad);
	}
	else if(action == AA_POSTDESTROY)
	{
		pthread_mutex_destroy(&ad->wpnmtx);
	}

	if(action == AA_CREATE || action == AA_CONFCHANGED)
	{ //this will not check same ships with different guns (overrides)
		ad->BulletDamageLevel=cfg->GetInt(a->cfg,"Bullet","BulletDamageLevel",200);
		ad->BulletDamageUpgrade=cfg->GetInt(a->cfg,"Bullet","BulletDamageUpgrade",100);
		ad->BulletAliveTime=cfg->GetInt(a->cfg,"Bullet","BulletAliveTime",550);
		ad->BulletExactDamage=cfg->GetInt(a->cfg,"Bullet","ExactDamage",0);
		ad->BombDamageLevel=cfg->GetInt(a->cfg,"Bomb","BombDamageLevel",750);
		ad->EBombDamagePercent=cfg->GetInt(a->cfg,"Bomb","EBombDamagePercent",1000);
		ad->BBombDamagePercent=cfg->GetInt(a->cfg,"Bomb","BBombDamagePercent",1000);
		ad->EBombShutdownTime=cfg->GetInt(a->cfg,"Bomb","EBombShutdownTime",0);
		ad->BombAliveTime=cfg->GetInt(a->cfg,"Bomb","BombAliveTime",8000);
		ad->BombExplodePixels=cfg->GetInt(a->cfg,"Bomb","BombExplodePixels",80);
		ad->ProximityDistance=cfg->GetInt(a->cfg,"Bomb","ProximityDistance",3);
		ad->BombExplodeDelay=cfg->GetInt(a->cfg,"Bomb","BombExplodeDelay",150);
		ad->MineAliveTime=cfg->GetInt(a->cfg,"Mine","MineAliveTime",12000);
		ad->BurstDamageLevel=cfg->GetInt(a->cfg,"Burst","BurstDamageLevel",500);
		ad->GravityBombs=cfg->GetInt(a->cfg,"Wormhole","GravityBombs",1);
		ad->RepelSpeed=cfg->GetInt(a->cfg,"Repel","RepelSpeed",5000);
		ad->RepelTime=cfg->GetInt(a->cfg,"Repel","RepelTime",225);
		ad->RepelDistance=cfg->GetInt(a->cfg,"Repel","RepelDistance",512);

		int s; for(s=0; s < 8; s++)
		{
			ad->ship[s].BulletSpeed=cfg->GetInt(a->cfg,cfg->SHIP_NAMES[s],"BulletSpeed",2000);
			ad->ship[s].MultiFireAngle=cfg->GetInt(a->cfg,cfg->SHIP_NAMES[s],"MultiFireAngle",0);
			ad->ship[s].BombSpeed=cfg->GetInt(a->cfg,cfg->SHIP_NAMES[s],"BombSpeed",2000);
			ad->ship[s].BombBounceCount=cfg->GetInt(a->cfg,cfg->SHIP_NAMES[s],"BombBounceCount",0);
			ad->ship[s].EmpBomb=cfg->GetInt(a->cfg,cfg->SHIP_NAMES[s],"EmpBomb",0);
			ad->ship[s].Radius=cfg->GetInt(a->cfg,cfg->SHIP_NAMES[s],"Radius",14);
			ad->ship[s].DoubleBarrel=cfg->GetInt(a->cfg,cfg->SHIP_NAMES[s],"DoubleBarrel",0);
			ad->ship[s].BurstSpeed=cfg->GetInt(a->cfg,cfg->SHIP_NAMES[s],"BurstSpeed",0);
			ad->ship[s].BurstShrapnel=cfg->GetInt(a->cfg,cfg->SHIP_NAMES[s],"BurstShrapnel",0);
// lm->Log(L_ERROR,"load: ship=%i bomb=%i bullet=%i",s,ad->ship[s].BombSpeed,ad->ship[s].BulletSpeed);
		}
	}
}

local void Pppk(Player *p, const struct C2SPosition *pos)
{
	Arena *a=p->arena;

	if(a && (p->status == S_PLAYING) && (p->pkt.ship != SHIP_SPEC) && (pos->x != -1) && (pos->y != -1))
	{
		adata *ad=P_ARENA_DATA(a,adkey);

		if(pos->weapon.type)
		{
			WEAPON_LOCK(ad);
			add_weapon(ad,p,pos);
			WEAPON_UNLOCK(ad);
		}
		else if(pos->status & STATUS_SAFEZONE)
		{
			WEAPON_LOCK(ad);
			remove_weapons(ad,p,REMOVEWEAPON_SAFEZONE);
			WEAPON_UNLOCK(ad);
		}
	}
}

local Ifakedamage damageint=
{
	INTERFACE_HEAD_INIT(I_FAKEDAMAGE,"fakedamage")

	AddFake, RemoveFake,
	AddArena, RemoveArena,
};

EXPORT const char info_fakedamage[]=
".\n"
"Fake Damage Module\n"
"By Smong + JoWie + Cheese\n"
"Makes fake players take damage. (also tiles)";

EXPORT int MM_fakedamage(int action, Imodman *mm2, Arena *a)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN,ALLARENAS);
		pd=mm->GetInterface(I_PLAYERDATA,ALLARENAS);
		cfg=mm->GetInterface(I_CONFIG,ALLARENAS);
		prng=mm->GetInterface(I_PRNG,ALLARENAS);
		aman=mm->GetInterface(I_ARENAMAN,ALLARENAS);
		map=mm->GetInterface(I_MAPDATA,ALLARENAS);
		ml=mm->GetInterface(I_MAINLOOP,ALLARENAS);
		if(!lm || !pd || !cfg || !prng || !aman || !map || !ml) return MM_FAIL;
		fake=mm->GetInterface(I_FAKE,ALLARENAS); //debug
		net=mm->GetInterface(I_NET,ALLARENAS); //debug
		
		adkey=aman->AllocateArenaData(sizeof(adata));
		if(adkey == -1) return MM_FAIL;

		//generate custom sine table
		int i; for(i=0; i < 40; i++)
		{
			sintabd[i]=sin(i * PI / 20.0f);
			sintab[i]=(int)(128 * sintabd[i]);
		}

		//if loaded after arenas have been created
		Arena *aa;
		Link *link;
		aman->Lock();
		FOR_EACH_ARENA(aa)
		{
			ArenaAction(aa,AA_PRECREATE);
			ArenaAction(aa,AA_CREATE);
		}
		aman->Unlock();
		
		mm->RegInterface(&damageint,ALLARENAS);

		mm->RegCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->RegCallback(CB_SHIPFREQCHANGE,ShipFreqChange,ALLARENAS);
		mm->RegCallback(CB_PLAYERACTION,PlayerAction,ALLARENAS);
		mm->RegCallback(CB_ARENAACTION,ArenaAction,ALLARENAS);
		mm->RegCallback(CB_PPK,Pppk,ALLARENAS);
		
		lm->Log(L_ERROR,"<fake> Fake Damage Module has loaded.");
		
		return MM_OK;
	}
	else if(action == MM_ATTACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake Damage Module has attached to the arena.");
		
lvz=mm->GetInterface(I_OBJECTS,ALLARENAS);
tgt.type=T_ARENA;
tgt.u.arena=a;
aaaa=a;
		if(fake && net)
		{
			DEBUG=TRUE;
			tester=fake->CreateFakePlayer("DamageTest",a,SHIP_SHARK,0);
		}
        
		return MM_OK;
	}
	else if(action == MM_DETACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake Damage Module has detached from the arena.");
        		
		if(fake && net)
		{
			fake->EndFaked(tester);
			tester=NULL;
			DEBUG=FALSE;
		}
mm->ReleaseInterface(lvz);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		lm->Log(L_ERROR,"<fake> Fake Damage Module has unloaded.");
		
		if(mm->UnregInterface(&damageint,ALLARENAS)) return MM_FAIL;

		mm->UnregCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->UnregCallback(CB_SHIPFREQCHANGE,ShipFreqChange,ALLARENAS);
		mm->UnregCallback(CB_PLAYERACTION,PlayerAction,ALLARENAS);
		mm->UnregCallback(CB_ARENAACTION,ArenaAction,ALLARENAS);
		mm->UnregCallback(CB_PPK,Pppk,ALLARENAS);
		
		//if unloaded while arenas are still running
		Arena *a;
		Link *link;
		aman->Lock();
		FOR_EACH_ARENA(a)
		{
			ArenaAction(a,AA_DESTROY);
			ArenaAction(a,AA_POSTDESTROY);
		}
		aman->Unlock();

		//if unloaded while bots are still flying
		BotData *bd;
		FOR_EACH_LINK(turrets,bd,l)
		{
			afree(bd);
		}
		LLEmpty(&turrets);
		LLEmpty(&aset);
		
		aman->FreeArenaData(adkey);
		
		mm->ReleaseInterface(fake); //debug
		mm->ReleaseInterface(net); //debug
		
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(prng);
		mm->ReleaseInterface(aman);
		mm->ReleaseInterface(map);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
