//Fake Energy Module
//By Cheese

/*
TODO:
make kill callback/respawn time work properly again
greens

REVISION HISTORY
Aug 30 2010 Cheese	Initial module creation
Jul 30 2012 Cheese	Source code released to public
Dec 11 2012 Cheese	Split FindFake from AddFake
						Added ModifyEnergy
*/

#include "asss.h"
#include "fakeenergy.h"
#include "fakedamage.h"

local Imodman *mm;
local Ilogman *lm;
local Ichat *chat;
local Icmdman *cmd;
local Iconfig *cfg;
local Imainloop *ml;
local Iarenaman *am;
local Igame *game;
local Iprng *prng;
local Ifakedamage *dmg;

local LinkedList turrets=LL_INITIALIZER;
local pthread_mutex_t energy_mtx=PTHREAD_MUTEX_INITIALIZER;

#define IS_PLAYER(p) ((p)->type == T_CONT)
#define IS_FAKE(p) ((p)->type == T_FAKE)
#define FOR_EACH_LINK(x,y,z) Link *(z); for((z)=LLGetHead(&(x)); (z) && (((y)=(z)->data, (z)=(z)->next) || 1); )
#define ENERGY_LOCK() pthread_mutex_lock(&energy_mtx);
#define ENERGY_UNLOCK() pthread_mutex_unlock(&energy_mtx);

typedef struct EnergyData
{
	Player *p;
	int state;
	int energy;
	int maxenergy;
	int recharge;
	int spawnx;
	int spawny;
	int spawnradius;
	ticks_t lastenergy;
	ticks_t endemp;
} EnergyData;

typedef struct adata
{
	int EnterDelay;
} adata;
local int adkey;

local void clearenergy(EnergyData *ed)
{
	if(dmg && ed->maxenergy) dmg->RemoveFake(ed->p);
	LLRemove(&turrets,ed);
	afree(ed);
}

local EnergyData* findfake(Player *p)
{
	if(!p) return NULL;
	if(!IS_FAKE(p)) return NULL;
	
	EnergyData *ed=NULL;
	ENERGY_LOCK();
	EnergyData *fed;
	FOR_EACH_LINK(turrets,fed,l)
	{
		if(fed->p == p) ed=fed;
	}
	ENERGY_UNLOCK();
	
	return ed;
}

local void AddFake(Player *p)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	EnergyData *ed=findfake(p);
	if(!ed)
	{
		ticks_t now=current_ticks();
		ed=amalloc(sizeof(EnergyData));
		
		ed->p=p;
		ed->energy=0;
		ed->maxenergy=0;
		ed->recharge=0;
		ed->endemp=now;
		ed->lastenergy=now;
		ed->spawnx=512*16;
		ed->spawny=512*16;
		ed->spawnradius=0;
		
		ENERGY_LOCK();
		LLAdd(&turrets,ed);
		ENERGY_UNLOCK();
	}
}

local void ChangeFake(Player *p, int energy, int recharge, int spawnx, int spawny, int spawnr)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	EnergyData *ed=findfake(p);
	if(!ed) return;
	
	if(energy >= 0)
	{
		ed->maxenergy=energy;
		if(ed->energy > ed->maxenergy) ed->energy=ed->maxenergy;
	}
	if(recharge >= 0) ed->recharge=recharge;
	if((spawnx > 0) && (spawnx < 1024)) ed->spawnx=spawnx;
	if((spawny > 0) && (spawny < 1024)) ed->spawny=spawny;
	if((spawnr > 0) && (spawnr < 1024)) ed->spawnradius=spawnr;
	
	ed->p->position.energy=ed->energy;
}

local void RemoveFake(Player *p)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	ENERGY_LOCK();
	EnergyData *ed;
	FOR_EACH_LINK(turrets,ed,l)
	{
		if(ed->p == p) clearenergy(ed);
	}
	ENERGY_UNLOCK();
}

local void FakeDamage(Player *p, Player *killer, int dist, int damage, int type, int level, BOOL bouncing, int emptime, void *vp);
local void SetState(Player *p, fe_state state, BOOL set)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	EnergyData *ed=findfake(p);
	if(!ed) return;
	
	BOOL dmgbefore=FALSE;
	if(ed->state & STATE_TAKEDAMAGE) dmgbefore=TRUE;
	
	if(set) ed->state |= state;
	else ed->state &= ~state;
	
	BOOL dmgafter=FALSE;
	if(ed->state & STATE_TAKEDAMAGE) dmgafter=TRUE;
	
	if(!dmgbefore && dmgafter) dmg->AddFake(p,FakeDamage,(void*)ed);
	else if(!dmgbefore && dmgafter) dmg->RemoveFake(p);
}

local void ModifyEnergy(Player *p, int newenergy)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	EnergyData *ed=findfake(p);
	if(!ed) return;
	
	if(newenergy >= 0) ed->energy=newenergy;
	if(ed->energy > ed->maxenergy) ed->energy=ed->maxenergy;
	
	ed->p->position.energy=ed->energy;
}

local int respawn_timer(void *vp)
{
	EnergyData *ed=(EnergyData*)vp;
	if(ed->p->pkt.ship == SHIP_SPEC) return;
	
	if(ed->spawnradius)
	{
		ed->p->position.x=prng->Number(ed->spawnx-ed->spawnradius,ed->spawnx+ed->spawnradius)*16;
		ed->p->position.y=prng->Number(ed->spawny-ed->spawnradius,ed->spawny+ed->spawnradius)*16;
		
		if(ed->p->position.x/16 < 0) ed->p->position.x=-ed->p->position.x;
		if(ed->p->position.y/16 < 0) ed->p->position.y=-ed->p->position.y;
		if(ed->p->position.x/16 > 1024) ed->p->position.x=1024*16-ed->p->position.x;
		if(ed->p->position.y/16 > 1024) ed->p->position.y=1024*16-ed->p->position.y;
	}
	
	ed->energy=ed->maxenergy;
	ed->p->position.energy=ed->energy;
	ed->p->flags.is_dead=FALSE;
	
	return 0;
}

local void FakeDamage(Player *p, Player *killer, int dist, int damage, int type, int level, BOOL bouncing, int emptime, void *vp)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	Arena *a=p->arena;
	ticks_t now=current_ticks();
	EnergyData *ed=(EnergyData*)vp;
	
// lm->Log(L_ERROR,"Damage: %s shot %s (%s) dmg=%i type=%i lvl=%i splash=%i",killer->name,p->name,(ed->p->flags.is_dead ? "dead" : "alive"),damage,type,level,dist);
	if(p->flags.is_dead) return;
	if((killer == p) && (ed->state & STATE_IGNORESELFDAMAGE)) return;
	if((killer->pkt.freq == p->pkt.freq) && (ed->state & STATE_IGNORETEAMDAMAGE)) return;
	if((p->position.status & STATUS_SAFEZONE) && !(ed->state & STATE_IGNORESAFETY)) return;
	
	ed->energy-=damage;
	ed->p->position.energy=ed->energy;
	
	
	if(emptime && !(ed->state & STATE_IGNOREEMP))
	{
		if(TICK_GT(now+emptime,ed->endemp)) ed->endemp=now+emptime;
		int maxemp=cfg->GetInt(p->arena->cfg,"Bomb","EBombShutdownTime",0);
		if(TICK_GT(ed->endemp,now+maxemp)) ed->endemp=now+maxemp;
	}
	
	if(ed->energy < 0)
	{
		ticks_t now=current_ticks();
		
		int pts=0, green=0;
		DO_CBS(CB_KILL,a,KillFunc,(a,killer,p,p->position.bounty,p->pkt.flagscarried,&pts,&green));
		game->FakeKill(killer,p,pts,p->pkt.flagscarried);
		lm->LogA(L_INFO,"fake",a,"[%s] killed by [%s] (bty=%d,flags=%d,pts=%d)",p->name,killer->name,p->position.bounty,p->pkt.flagscarried,pts);

		p->flags.is_dead=TRUE;
		p->position.x=-1;
		p->position.y=-1;
		p->position.xspeed=0;
		p->position.yspeed=0;

		adata *ad=P_ARENA_DATA(a,adkey);
		ml->SetTimer(respawn_timer,ad->EnterDelay,100,ed,0);
	}
}

local void mlfunc()
{
	ENERGY_LOCK();
	EnergyData *ed;
	FOR_EACH_LINK(turrets,ed,l)
	{
		ticks_t now=current_ticks();
		
		if(!ed->p || !ed->p->arena)
		{
			clearenergy(ed);
			continue;
		}
		
		if(!ed->p->flags.is_dead)
		{
			ed->energy=ed->p->position.energy;
			
			if(ed->energy < 0) ed->energy=0;
			if((ed->energy < ed->maxenergy) && TICK_GT(now,ed->endemp)) ed->energy+=ed->recharge*TICK_DIFF(now,ed->lastenergy)/1000;
			if(ed->energy > ed->maxenergy) ed->energy=ed->maxenergy;
			
			ed->p->position.energy=ed->energy;
		}
		ed->lastenergy=now;
	}
	ENERGY_UNLOCK();
}

local void NewPlayer(Player *p, int isnew)
{
	if(isnew) return;
	if(IS_FAKE(p)) RemoveFake(p);
}

local void ArenaAction(Arena *a, int action)
{
	if(action == AA_CREATE || action == AA_CONFCHANGED)
	{
		adata *ad=P_ARENA_DATA(a,adkey);
		ad->EnterDelay=cfg->GetInt(a->cfg,"Kill","EnterDelay",100);
	}
}

local helptext_t cmd_fakeenergy=
"Targets: Fake player\n"
"Energy args:\n"
"[nrg=#] Energy\n"
"[chg=#] Recharge\n"
"[spx=#] Spawn X\n"
"[spy=#] Spawn Y\n"
"[spr=#] Spawn radius\n"
"Manages energy for fake players.";

local void fakeenergy(const char *command, const char *params, Player *p, const Target *t)
{
	if(t->type != T_PLAYER) chat->SendMessage(p,"Invalid target.");
	else
	{
		if(t->u.p->type != T_FAKE) chat->SendMessage(p,"Only fakes can be turrets.");
		else
		{
			EnergyData *ed=findfake(t->u.p);
			if(!ed)
			{
				AddFake(t->u.p);
				ed=findfake(t->u.p);
				if(!ed) return;
			}
			
			char buf[255];
			char *word=NULL;
			const char *tmp=NULL;
			while(strsplit(params," ,:",buf,sizeof(buf),&tmp))
			{
				word=buf;
				if(!strnicmp(word,"nrg=",4))
				{
					word+=4;
					int check=atoi(word);
					if(!ed->energy && dmg && check) dmg->AddFake(t->u.p,FakeDamage,(void*)ed);
					if(check)
					{
						ed->energy=check;
						ed->maxenergy=check;
					}
					else
					{
						if(ed->energy && dmg) dmg->RemoveFake(t->u.p);
						ed->energy=0;
						ed->maxenergy=0;
					}
				}
				else if(!strnicmp(word,"chg=",4))
				{
					word+=4;
					int check=atoi(word);
					if(check) ed->recharge=check;
				}
				else if(!strnicmp(word,"spx=",4))
				{
					word+=4;
					int check=atoi(word);
					if(check) ed->spawnx=check;
				}
				else if(!strnicmp(word,"spy=",4))
				{
					word+=4;
					int check=atoi(word);
					if(check) ed->spawny=check;
				}
				else if(!strnicmp(word,"spr=",4))
				{
					word+=4;
					int check=atoi(word);
					ed->spawnradius=check;
				}
			}
			chat->SendMessage(p,"%s modified.",t->u.p->name);
		}
	}
}

local Ifakeenergy energyint=
{
	INTERFACE_HEAD_INIT(I_FAKEENERGY,"fakeenergy")
	
	AddFake, ChangeFake, RemoveFake,
	SetState,
	ModifyEnergy,
};

EXPORT const char info_fakeenergy[]=
".\n"
"Fake Weapons Module\n"
"by Cheese\n"
"Makes fake players have energy.";

EXPORT int MM_fakeenergy(int action, Imodman *mm2, Arena *a)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN,ALLARENAS);
		chat=mm->GetInterface(I_CHAT,ALLARENAS);
		cmd=mm->GetInterface(I_CMDMAN,ALLARENAS);
		cfg=mm->GetInterface(I_CONFIG,ALLARENAS);
		ml=mm->GetInterface(I_MAINLOOP,ALLARENAS);
		am=mm->GetInterface(I_ARENAMAN,ALLARENAS);
		game=mm->GetInterface(I_GAME,ALLARENAS);
		prng=mm->GetInterface(I_PRNG,ALLARENAS);
		if(!lm || !chat || !cmd || !cfg || !ml || !am || !game || !prng) return MM_FAIL;
		
		dmg=mm->GetInterface(I_FAKEDAMAGE,ALLARENAS);
		if(!dmg) lm->Log(L_ERROR,"<fakeenergy> Could not find fakedamage, bots will not take weapon damage.");
		
		adkey=am->AllocateArenaData(sizeof(adata));
		if(adkey == -1) return MM_FAIL;
		
		mm->RegInterface(&energyint,ALLARENAS);
		
		mm->RegCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->RegCallback(CB_NEWPLAYER,NewPlayer,ALLARENAS);
		mm->RegCallback(CB_ARENAACTION,ArenaAction,ALLARENAS);
		
		cmd->AddCommand("fe",fakeenergy,ALLARENAS,cmd_fakeenergy);
		
		lm->Log(L_ERROR,"<fake> Fake Energy Module has loaded.");
		
		return MM_OK;
	}
	else if(action == MM_ATTACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake Energy Module has attached to the arena.");
        
		return MM_OK;
	}
	else if(action == MM_DETACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake Energy Module has detached from the arena.");
        
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		lm->Log(L_ERROR,"<fake> Fake Energy Module has unloaded.");
		
		if(mm->UnregInterface(&energyint,ALLARENAS)) return MM_FAIL;
		
		cmd->RemoveCommand("fe",fakeenergy,ALLARENAS);
		
		mm->UnregCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->UnregCallback(CB_NEWPLAYER,NewPlayer,ALLARENAS);
		mm->UnregCallback(CB_ARENAACTION,ArenaAction,ALLARENAS);
		
		am->FreeArenaData(adkey);
		LLEmpty(&turrets);
		
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(game);
		mm->ReleaseInterface(prng);
		mm->ReleaseInterface(dmg);
		
		return MM_OK;
	}
	return MM_FAIL;
}
