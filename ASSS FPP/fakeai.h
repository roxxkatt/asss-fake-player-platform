#ifndef __FAKEAI_H
#define __FAKEAI_H

#include "linuxdeps.h"

typedef struct Cell
{
	struct Cell *parent; //null if root
	struct Cell *children[9]; //null if wall or in tree already
	int x;
	int y;
	int cost;
} Cell;

typedef enum
{
	STATE_UNTASKEDSTOP	=1,
	STATE_PATHFINDING	=2,
//	STATE_TOUCHFLAGS	=4,
//	STATE_TOUCHBALLS	=8,
//	STATE_TOUCHGREENS	=16,
} fai_state;

#define I_FAKEAI "Ifakeai-1"
typedef struct Ifakeai
{
	INTERFACE_HEAD_DECL
	
	void (*AddFake)(Player *p);
	void (*ChangeFake)(Player *p);
	void (*RemoveFake)(Player *p);
	
	void (*SetState)(Player *p, fai_state state, BOOL set);
	void (*MoveToTarget)(Player *p, int x, int y); //tiles
	void (*MoveToPlayer)(Player *p, Player *target);
	//void (*MoveAwayFromPlayer)(Player *p, Player *target, BOOL stopafter);
	//void (*MoveAwayFromTarget)(Player *p, int x, int y);
	
	BOOL (*GetWaypoints)(LinkedList *list, Arena *a, int srcx, int srcy, int dstx, int dsty);
	//fills list with cells, returns true if destination is accessible.
	//beware, computation cost increases exponentially with distance

} Ifakeai;

#endif
