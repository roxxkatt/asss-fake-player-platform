//Fake AI Module
//By Cheese

/*
TODO:
shipradius in pathfinding
adaptive pathfinding
reduce cpu impact
jump point search A*

REVISION HISTORY
Aug 30 2010 Cheese	Initial module creation
Jul 30 2012 Cheese	Source code released to public
Dec 11 2012 Cheese	Split FindFake from AddFake
*/

#include "asss.h"
#include "fakeai.h"
#include "fakemove.h"

local Imodman *mm;
local Ilogman *lm;
local Iplayerdata *pd;
local Icmdman *cmd;
local Ichat *chat;
local Imapdata *map;
local Ifakemove *fm;

local LinkedList turrets=LL_INITIALIZER;
local LinkedList astar=LL_INITIALIZER;
local pthread_mutex_t ai_mtx=PTHREAD_MUTEX_INITIALIZER;
//local pthread_mutex_t path_mtx=PTHREAD_MUTEX_INITIALIZER;

#define IS_PLAYER(p) ((p)->type == T_CONT)
#define IS_FAKE(p) ((p)->type == T_FAKE)
#define FOR_EACH_LINK(x,y,z) Link *(z); for((z)=LLGetHead(&(x)); (z) && (((y)=(z)->data, (z)=(z)->next) || 1); )
#define IS_TILE_SOLID(tile) ((tile >= 1 && tile <= 161) || (tile >= 192 && tile <= 240) || (tile >= 243 && tile <= 251))
#define AI_LOCK() pthread_mutex_lock(&ai_mtx);
#define AI_UNLOCK() pthread_mutex_unlock(&ai_mtx);
#define PATH_LOCK() pthread_mutex_lock(&aid->path_mtx);
#define PATH_UNLOCK() pthread_mutex_unlock(&aid->path_mtx);

#include "objects.h"
local Iobjects *lvz;
local LinkedList objs=LL_INITIALIZER;
#define white 15000
#define red 16000
#define green 17000
#define blue 18000
Target tgt;
Arena *aaa=NULL;
int whites=0;
int reds=0;
int greens=0;
int blues=0;

typedef struct AIData
{
	Player *p;
	int state;
	Player *target;
	int range;
	BOOL closest;
	int x;
	int y;
	LinkedList path;
	ticks_t nextpath;
	pthread_mutex_t path_mtx;
} AIData;

static long lhypot(register long dx, register long dy)
{
	register unsigned long r, dd;
	dd=dx*dx+dy*dy;
	if(dx < 0) dx=-dx;
	if(dy < 0) dy=-dy;
	r=(dx > dy) ? (dx+(dy>>1)) : (dy+(dx>>1));
	if(r == 0) return (long)r;
	r=(dd/r+r)>>1;
	r=(dd/r+r)>>1;
	r=(dd/r+r)>>1;
	return (long)r;
}

local Player* fuzzyfind(Arena *a, char* name)
{
	if(!a || !name) return NULL;
	Player *pp=NULL;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(pp,a)
	{
		if(!stricmp(pp->name,name)) goto found;
	}
	FOR_EACH_PLAYER_IN_ARENA(pp,a)
	{
		int len=strlen(name);
		if(len > strlen(pp->name)) continue;
		if(!strnicmp(pp->name,name,len)) goto found;
	}
	pd->Unlock();
	return NULL;
	
found:
	pd->Unlock();
	return pp;
}

local Player* findTurretTarget(AIData *aid)
{
	Player *tp=NULL;
	int bd=aid->range*16;

	int x=aid->p->position.x;
	int y=aid->p->position.y;
	if(x <= 0) x=-x+32;
	if(y <= 0) y=-y+32;
	
	Player *pp;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(pp,aid->p->arena)
	{
		if(pp->pkt.freq == aid->p->pkt.freq) continue;
		if(pp->pkt.ship == SHIP_SPEC) continue;
		if(pp->flags.is_dead) continue;
		
		int x1=pp->position.x;
		int y1=pp->position.y;
		if(x1 <= 0) x1=-x1;
		if(y1 <= 0) y1=-y1;
		int dist=lhypot(x-x1,y-y1);
		if(dist < bd)
		{
			tp=pp;
			bd=dist;
		}
	}
	pd->Unlock();
	return tp;
}

local void clearai(AIData *aid)
{
	LLRemove(&turrets,aid);
	PATH_LOCK();
	LLEnum(&aid->path,afree);
	LLEmpty(&aid->path);
	PATH_UNLOCK();
	afree(aid);
}

local AIData* findfake(Player *p)
{
	if(!p) return NULL;
	if(!IS_FAKE(p)) return NULL;
	
	AIData *aid=NULL;
	
	AI_LOCK();
	AIData *faid;
	FOR_EACH_LINK(turrets,faid,l)
	{
		if(faid->p == p) aid=faid;
	}
	AI_UNLOCK();
	
	return aid;
}

local void AddFake(Player *p)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	AIData *aid=findfake(p);
	if(!aid)
	{
		aid=amalloc(sizeof(AIData));
		
		aid->p=p;
		aid->target=NULL;
		aid->range=100;
		aid->closest=FALSE;
		aid->nextpath=current_ticks();
		LLInit(&aid->path);
		pthread_mutex_init(&aid->path_mtx,NULL);
		
		AI_LOCK();
		LLAdd(&turrets,aid);
		AI_UNLOCK();
	}
}

local void ChangeFake(Player *p)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	// AIData *aid=findfake(p);
	// if(!aid) return;
}

local void RemoveFake(Player *p)
{
	AI_LOCK();
	AIData *aid;
	FOR_EACH_LINK(turrets,aid,l)
	{
		if(aid->p == p) clearai(aid);
	}
	AI_UNLOCK();
}

local void SetState(Player *p, fai_state state, BOOL set)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	AIData *aid=findfake(p);
	if(!aid) return;
	
	if(set) aid->state |= state;
	else aid->state &= ~state;
}

void MoveToTarget(Player *p, int x, int y)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	if((x < 0) || (x > 1024) || (y < 0) || (y > 1024)) return;
	
	AIData *aid=findfake(p);
	if(!aid) return;
	
	aid->x=x;
	aid->y=y;
}

void MoveToPlayer(Player *p, Player *target)
{
	if(!p) return;
	if(!target) return;
	if(!IS_FAKE(p)) return;
	
	AIData *aid=findfake(p);
	if(!aid) return;
	
	aid->target=target;
}

local BOOL GetWaypoints(LinkedList *path, Arena *a, int srcx, int srcy, int dstx, int dsty)
{
	if(!path || !a) return FALSE;
	if((dstx < 0) || (dstx > 1024)) return FALSE;
	if((dsty < 0) || (dsty > 1024)) return FALSE;
	
int num=0;
if(aaa)
{
num=green+greens;
lvz->Move(&tgt,num,srcx*16,srcy*16,0,0);
// lm->Log(L_ERROR,"Green %d on at x=%d y=%d (%d,%d)",num,srcx*16,srcy*16,srcx,srcy);
// lvz->Image(&tgt,num,
lvz->Toggle(&tgt,num,1);
greens++; if(greens > 1000) greens=0;

num=green+greens;
lvz->Move(&tgt,num,dstx*16,dsty*16,0,0);
lvz->Toggle(&tgt,num,1);
greens++; if(greens > 1000) greens=0;
}

	LinkedList open=LL_INITIALIZER;
	LinkedList closed=LL_INITIALIZER;
	LLEnum(path,afree);
	LLEmpty(path);
	
	Cell *c3=amalloc(sizeof(Cell));
	c3->x=srcx;
	c3->y=srcy;
	// c3->cost=0;
c3->cost=lhypot((srcx-dstx),(srcy-dsty));
	LLAddFirst(&open,c3);
	
if(aaa)
{
num=white+whites;
lvz->Move(&tgt,num,c3->x*16,c3->y*16,0,0);
lvz->Toggle(&tgt,num,1);
whites++; if(whites > 1000) whites=0;
// LLAddFirst(&objs,num);
}

int safety=0;
	while(!LLIsEmpty(&open))
	{
		Cell *c=NULL;
		
		int cost=100000000;
		Cell *fc;
		FOR_EACH_LINK(open,fc,l)
		{
int estcost=fc->cost+lhypot((fc->x-dstx),(fc->y-dsty));
			if(estcost < cost)
			{
				c=fc;
				cost=estcost;
			}
		}
		//if(!c) OH SHI-
		
		LLRemove(&open,c);
		LLAddFirst(&closed,c);
	
if(aaa)
{
num=red+reds;
lvz->Move(&tgt,num,c->x*16,c->y*16,0,0);
lvz->Toggle(&tgt,num,1);
reds++; if(reds > 1000) reds=0;
// LLAddFirst(&objs,num);
}

		if((c->x == dstx) && (c->y == dsty))
		{
			while(c)
			{
				Cell *nc=amalloc(sizeof(Cell));
				memcpy(nc,c,sizeof(Cell));
				LLAddFirst(path,nc);
				c=c->parent;
			}
			
			LLEnum(&open,afree);
			LLEnum(&closed,afree);
			LLEmpty(&open);
			LLEmpty(&closed);
			return TRUE;
		}
		
		int ii=0;
		int y; for(y=c->y-1; y<=c->y+1; y++)
		{
			int x; for(x=c->x-1; x<=c->x+1; x++)
			{
				if((x == c->x) && (y == c->y)) continue; //this one
				if( ((x == c->x-1) && (y == c->y-1)) ||
				((x == c->x-1) && (y == c->y+1)) ||
				((x == c->x+1) && (y == c->y-1)) ||
				((x == c->x+1) && (y == c->y+1)) ) continue; //cost2+=4; //no diagonals
				if((x < 0) || (x > 1024) || (y < 0) || (y > 1024)) continue; //out of map
				if(IS_TILE_SOLID(map->GetTile(a,x,y))) continue;
				
				BOOL inclosedset=FALSE;
				FOR_EACH_LINK(closed,fc,l)
				{
					if((x == fc->x) && (y == fc->y))
					{
						inclosedset=TRUE;
						break;
					}
				}
				if(inclosedset) continue;
				
				Cell *d=NULL;
				FOR_EACH_LINK(open,fc,ll)
				{
					if((x == fc->x) && (y == fc->y))
					{
						d=fc;
						break;
					}
				}
				
				// int pathcost=c->cost+1+lhypot((x-dstx),(y-dsty))*100;
			
				if(!d)
				{
					Cell *cc=amalloc(sizeof(Cell));
					c->children[ii]=cc;
					cc->parent=c;
					cc->x=x;
					cc->y=y;
					cc->cost=c->cost+1;
					LLAddFirst(&open,cc);
					d=cc;
		
if(aaa)
{			
num=white+whites;
lvz->Move(&tgt,num,cc->x*16,cc->y*16,0,0);
lvz->Toggle(&tgt,num,1);
whites++; if(whites > 1000) whites=0;
// LLAddFirst(&objs,num);
}

				}
				else if(c->cost+1 < d->cost)
				{
					d->parent=c;
					d->cost=c->cost+1;
				}
				
				ii++;
safety++;
if(safety > 1000) break;
			}
if(safety > 1000) break;
		}
if(safety > 1000) break;
	}
if(safety > 1000) lm->Log(L_ERROR,"Computation took too long. Computation halted.");
	
	LLEnum(&open,afree);
	LLEnum(&closed,afree);
	LLEmpty(&open);
	LLEmpty(&closed);
	return FALSE;
}

local void mlfunc()
{
	AI_LOCK();
	AIData *aid;
	FOR_EACH_LINK(turrets,aid,l)
	{
		ticks_t now=current_ticks();
		
		if(!aid->p || !aid->p->arena)
		{
			clearai(aid);
			continue;
		}
		
		int x=0;
		int y=0;
		
		if(aid->x && aid->y)
		{
			x=aid->x;
			y=aid->y;
		}
		else
		{
			if(aid->target && (aid->target->arena != aid->p->arena)) aid->target=NULL;
			
			Player *tp=NULL;
			if(aid->target) tp=aid->target;
			else if(aid->closest) tp=findTurretTarget(aid);
			if(tp)
			{
				x=tp->position.x/16;
				y=tp->position.y/16;
			}
		}
		
		if(x && y)
		{
			if(aid->state & STATE_PATHFINDING)
			{
				if(TICK_GT(now,aid->nextpath) && !aid->p->flags.is_dead)
				{
lm->Log(L_ERROR,"Recalculating path...");
					PATH_LOCK();
					GetWaypoints(&aid->path,aid->p->arena,aid->p->position.x/16,aid->p->position.y/16,x,y);
					//aid->nextpath=now+lhypot(aid->p->position.x/16-x,aid->p->position.y/16-y)*5*10;
					aid->nextpath=now+60*60*1000;
					
					Link *l=LLGetHead(&aid->path);
					Cell *c=NULL;
					if(l) c=l->data;
					if(c)
					{
						x=c->x;
						y=c->y;
						aid->x=x;
						aid->y=y;
					}
					PATH_UNLOCK();
					
lm->Log(L_ERROR,"Starting to (%i,%i)",x,y);
					// fm->SetMoveTarget(aid->p,x,y);
				}
			}
		
			fm->SetMoveTarget(aid->p,x,y);
		}
		else if(aid->state & STATE_UNTASKEDSTOP) fm->SetMoveTarget(aid->p,0,0);
	}
	AI_UNLOCK();
}

local TargetReached(Player *p, int x, int y)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	AIData *aid=findfake(p);
	if(!aid) return;
	
	if(aid->state & STATE_PATHFINDING)
	{
lm->Log(L_ERROR,"Reached (%i,%i)",x,y);
		int nx=0;
		int ny=0;
if(aaa)
{
int num=blue+blues;
lvz->Move(&tgt,num,x*16,y*16,0,0);
lvz->Toggle(&tgt,num,1);
blues++; if(blues > 1000) blues=0;
}
		PATH_LOCK();
		Cell *c;
		FOR_EACH_LINK(aid->path,c,l)
		{
			if((c->x == x) && (c->y == y))
			{
				if(l) //next link
				{
					Cell *nc=l->data;
lm->Log(L_ERROR,"Moving to (%i,%i)",nc->x,nc->y);
					nx=nc->x;
					ny=nc->y;
				}
				
				LLRemove(&aid->path,c);
				afree(c);
				break;
			}
		}
		PATH_UNLOCK();
		
		aid->x=nx;
		aid->y=ny;
		fm->SetMoveTarget(p,nx,ny);
	}
}

local void NewPlayer(Player *p, int isnew)
{
	if(!p) return;
	if(isnew) return;
	if(IS_FAKE(p)) RemoveFake(p);
	
	AI_LOCK();
	AIData *aid;
	FOR_EACH_LINK(turrets,aid,l)
	{
		if(aid->target == p) aid->target=NULL;
	}
	AI_UNLOCK();
}

local helptext_t cmd_fakeai=
"Targets: Fake player\n"
"AI args:\n"
"[p=name] Player to follow\n"
"[r=#] Detection radius\n"
"[-c] Follow closest player\n"
"Displays commands dealing with fake players.";

local void fakeai(const char *command, const char *params, Player *p, const Target *t)
{
	if(t->type != T_PLAYER) chat->SendMessage(p,"Invalid target.");
	else
	{
		if(t->u.p->type != T_FAKE) chat->SendMessage(p,"Only fakes can be turrets.");
		else
		{
			AIData *aid=findfake(t->u.p);
			if(!aid)
			{
				AddFake(t->u.p);
				aid=findfake(t->u.p);
				if(!aid) return;
			}
			
			ticks_t now=current_ticks();
			
			char buf[255];
			char *word=NULL;
			const char *tmp=NULL;
			while(strsplit(params," ,:",buf,sizeof(buf),&tmp))
			{
				word=buf;
				if(!strnicmp(word,"p=",2))
				{
					word+=2;
					aid->target=fuzzyfind(p->arena,word);
					aid->nextpath=now;
				}
				else if(!strnicmp(word,"r=",2))
				{
					word+=2;
					int check=atoi(word);
					if(check) aid->range=check;
				}
				else if(!stricmp(word,"-c"))
				{
					aid->closest=!aid->closest;
					aid->nextpath=now;
				}
				else if(!stricmp(word,"-w"))
				{
					int off=TRUE;
					if(aid->state & STATE_PATHFINDING) off=FALSE;
					SetState(t->u.p,STATE_PATHFINDING,off);
if(aid->state & STATE_PATHFINDING)
lm->Log(L_ERROR,"Pathfinding ON");
else
lm->Log(L_ERROR,"Pathfinding OFF");
				}
				else if(!stricmp(word,"-s"))
				{
					int off=TRUE;
					if(aid->state & STATE_UNTASKEDSTOP) off=FALSE;
					SetState(t->u.p,STATE_UNTASKEDSTOP,off);
				}
				else if(!stricmp(word,"-go"))
				{
					PATH_LOCK();
					if(GetWaypoints(&aid->path,p->arena,t->u.p->position.x/16,t->u.p->position.y/16,500,500))
lm->Log(L_ERROR,"found!");
else
lm->Log(L_ERROR,"didnt find! :(");
					PATH_UNLOCK();
				}
				else if(!stricmp(word,"go"))
				{
					aid->nextpath=now;
				}
				else if(!stricmp(word,"path"))
				{
					PATH_LOCK();
					int n=0;
					Cell *c;
					FOR_EACH_LINK(aid->path,c,l)
					{
if(aaa)
{
int num=blue+blues;
lvz->Move(&tgt,num,c->x*16,c->y*16,0,0);
lvz->Toggle(&tgt,num,1);
blues++; if(blues > 1000) blues=0;
}

						// lm->Log(L_ERROR,"(%i,%i) cost=%i",c->x,c->y,c->cost);
						n++;
					}
					lm->Log(L_ERROR,"n=%i",n);
					PATH_UNLOCK();
				}
				else if(!stricmp(word,"path2"))
				{
					PATH_LOCK();
					int n=0;
					Cell *c;
					FOR_EACH_LINK(aid->path,c,l)
					{

						lm->Log(L_ERROR,"(%i,%i) cost=%i",c->x,c->y,c->cost);
						n++;
					}
					lm->Log(L_ERROR,"n=%i",n);
					PATH_UNLOCK();
				}
			}
			chat->SendMessage(p,"%s modified.",t->u.p->name);
		}
	}
}

local Ifakeai aiint=
{
	INTERFACE_HEAD_INIT(I_FAKEAI,"fakeai")
	
	AddFake, ChangeFake, RemoveFake,
	SetState, MoveToTarget, MoveToPlayer, GetWaypoints,
};

EXPORT const char info_fakeai[]=
".\n"
"Fake AI Module\n"
"by Cheese\n"
"Makes fake players smarter.";

EXPORT int MM_fakeai(int action, Imodman *mm2, Arena *a)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN,ALLARENAS);
		cmd=mm->GetInterface(I_CMDMAN,ALLARENAS);
		chat=mm->GetInterface(I_CHAT,ALLARENAS);
		pd=mm->GetInterface(I_PLAYERDATA,ALLARENAS);
		map=mm->GetInterface(I_MAPDATA,ALLARENAS);
		fm=mm->GetInterface(I_FAKEMOVE,ALLARENAS);
lvz=mm->GetInterface(I_OBJECTS,ALLARENAS);
		if(!lm || !cmd || !chat || !pd || !map || !fm) return MM_FAIL;
		
		mm->RegInterface(&aiint,ALLARENAS);
		
		mm->RegCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->RegCallback(CB_NEWPLAYER,NewPlayer,ALLARENAS);
		mm->RegCallback(CB_TARGETREACHED,TargetReached,ALLARENAS);
		
		cmd->AddCommand("fai",fakeai,ALLARENAS,cmd_fakeai);
		
		lm->Log(L_ERROR,"<fake> Fake AI Module has loaded.");
		
		return MM_OK;
	}
	else if(action == MM_ATTACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake AI Module has attached to the arena.");
		
tgt.type=T_ARENA;
tgt.u.arena=a;
aaa=a;
        
		return MM_OK;
	}
	else if(action == MM_DETACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake AI Module has detached from the arena.");
        
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		lm->Log(L_ERROR,"<fake> Fake AI Module has unloaded.");
		
		if(mm->UnregInterface(&aiint,ALLARENAS)) return MM_FAIL;
		
		cmd->RemoveCommand("fai",fakeai,ALLARENAS);
		
		mm->UnregCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->UnregCallback(CB_NEWPLAYER,NewPlayer,ALLARENAS);
		mm->UnregCallback(CB_TARGETREACHED,TargetReached,ALLARENAS);
		
mm->ReleaseInterface(lvz);
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(map);
		mm->ReleaseInterface(fm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
