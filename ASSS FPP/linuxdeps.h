#ifndef __LINUXDEPS_H
#define __LINUXDEPS_H

#define BOOL int

#ifndef WIN32
#define stricmp strcasecmp
#define strnicmp strncasecmp
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#endif


#endif
