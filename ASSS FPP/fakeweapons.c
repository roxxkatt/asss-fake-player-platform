//Fake Weapons Module
//By Cheese

/*
TODO:
allow custom aim targets

REVISION HISTORY
Aug 30 2010 Cheese	Initial module creation
Jul 30 2012 Cheese	Source code released to public
Dec 11 2012 Cheese	Split FindFake from AddFake
*/

#include "asss.h"
#include "fakeweapons.h"
//#include "fakeenergy.h"
//#include "fakemove.h"

local Imodman *mm;
local Ilogman *lm;
local Iplayerdata *pd;
local Icmdman *cmd;
local Ichat *chat;
local Igame *game;
local Iconfig *cfg;
//local Ifake *fake;
//local Ifakeenergy *nrg;
//local Ifakemove *move;

local LinkedList turrets=LL_INITIALIZER;
local LinkedList weapons=LL_INITIALIZER;
local pthread_mutex_t turret_mtx=PTHREAD_MUTEX_INITIALIZER;
local pthread_mutex_t weapon_mtx=PTHREAD_MUTEX_INITIALIZER;
int gwid=1;
ticks_t lastml;

#define PI 3.1415926535897932384626433832795f
#define IS_PLAYER(p) ((p)->type == T_CONT)
#define IS_FAKE(p) ((p)->type == T_FAKE)
#define FOR_EACH_LINK(x,y,z) Link *(z); for((z)=LLGetHead(&(x)); (z) && (((y)=(z)->data, (z)=(z)->next) || 1); )
#define TURRET_LOCK() pthread_mutex_lock(&turret_mtx);
#define TURRET_UNLOCK() pthread_mutex_unlock(&turret_mtx);
#define WEAPON_LOCK() pthread_mutex_lock(&weapon_mtx);
#define WEAPON_UNLOCK() pthread_mutex_unlock(&weapon_mtx);

typedef struct TurretData
{
	Player *p;
	struct C2SPosition pos;
	BOOL isdead;
	ticks_t endtime;
	ticks_t tosend;
	LinkedList weaponlist;
} TurretData;

typedef struct WeaponData
{
	int id;
	int range;
	BOOL killfakes;
	int firedelay;
	
	int weapon;
	int level;
	int shrap;
	int shraplevel;
	BOOL bounceshrap;
	BOOL alt;
} WeaponData;

typedef struct WeaponInfo
{
	int id;
	ticks_t tofire;
} WeaponInfo;

static long lhypot(register long dx, register long dy)
{
	register unsigned long r, dd;
	dd=dx*dx+dy*dy;
	if(dx < 0) dx=-dx;
	if(dy < 0) dy=-dy;
	r=(dx > dy) ? (dx+(dy>>1)) : (dy+(dx>>1));
	if(r == 0) return (long)r;
	r=(dd/r+r)>>1;
	r=(dd/r+r)>>1;
	r=(dd/r+r)>>1;
	return (long)r;
}

static inline unsigned char fireControl(Player *p, WeaponData *wd, Player *tp)
{
	double srcx=p->position.x;
	double srcy=p->position.y;
	double dstx=tp->position.x;
	double dsty=tp->position.y;
	double bestdx=dstx-srcx;
	double bestdy=dsty-srcy;
	double dxspeed=tp->position.xspeed - p->position.xspeed;
	double dyspeed=tp->position.yspeed - p->position.yspeed;
	double projspeed=cfg->GetInt(p->arena->cfg,cfg->SHIP_NAMES[p->pkt.ship],((wd->weapon == W_BULLET || wd->weapon == W_BOUNCEBULLET) ? "BulletSpeed" : "BombSpeed"),10);
	
	double dx, dy, err, pt;
	double besterr=20000;
	double tt=10;
	
	do
	{
		dx=(dstx+dxspeed*tt/1000)-srcx;
		dy=(dsty+dyspeed*tt/1000)-srcy;
		pt=lhypot(dx,dy)*1000/projspeed;
		
		err=abs(pt-tt);
		if(err < besterr)
		{
			besterr=err;
			bestdx=dx;
			bestdy=dy;
		}
		else if(err > besterr) break;
		
		tt+=10;
	}
	while(pt > tt && tt <= 250);
	
	return (int)round((atan2(bestdy,bestdx)+PI)*40.0/(2.0*PI)+30) % 40;
}

local Player* findTurretTarget(Player *p, WeaponData *wd)
{
	Player *tp=NULL;
	int bd=wd->range*16;

	int x=p->position.x;
	int y=p->position.y;
	if(x <= 0) x=-x+32;
	if(y <= 0) y=-y+32;
	
	Player *pp;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER_IN_ARENA(pp,p->arena)
	{
		if(pp->type == T_FAKE) if(!wd->killfakes) continue;
		if(pp->pkt.freq == p->pkt.freq) continue;
		if(pp->pkt.ship == SHIP_SPEC) continue;
		if(pp->flags.is_dead) continue;
		
		int x1=pp->position.x;
		int y1=pp->position.y;
		if(x1 <= 0) x1=-x1;
		if(y1 <= 0) y1=-y1;
		int dist=lhypot(x-x1,y-y1);
		if(dist < bd)
		{
			tp=pp;
			bd=dist;
		}
	}
	pd->Unlock();
	return tp;
}

local void clearweapon(WeaponData *wd)
{
	LLRemove(&weapons,wd);
	afree(wd);
}

local void clearweaponinfo(LinkedList *ll, WeaponInfo *wi)
{
	LLRemove(ll,wi);
	afree(wi);
}

local void clearturret(TurretData *td)
{
	game->SetShipAndFreq(td->p,SHIP_SPEC,td->p->arena->specfreq);
	
	WeaponInfo *wi;
	FOR_EACH_LINK(td->weaponlist,wi,l)
	{
		clearweaponinfo(&td->weaponlist,wi);
	}
	
	LLRemove(&turrets,td);
	afree(td);
}

local void updatepos(TurretData *td)
{
	ticks_t now=current_ticks();
	int bty=(td->endtime ? TICK_DIFF(td->endtime,now)/100 : 13373);
	int nrg=(td->p->position.energy ? td->p->position.energy : 13373);
	
	td->p->position.bounty=bty;
	td->p->position.energy=nrg;
	td->pos.extra.energy=nrg;
	td->pos.extra.timer=bty;
	
	td->pos.energy=td->p->position.energy;
	td->pos.extra.energy=td->p->position.energy;
	td->pos.bounty=td->p->position.bounty;
	td->pos.status=td->p->position.status;
	
	td->pos.x=td->p->position.x;
	td->pos.y=td->p->position.y;
	td->pos.xspeed=td->p->position.xspeed;
	td->pos.yspeed=td->p->position.yspeed;
	td->pos.rotation=td->p->position.rotation;
}

local TurretData* findfake(Player *p)
{
	if(!p) return NULL;
	if(!IS_FAKE(p)) return NULL;
	
	TurretData *td=NULL;
	TURRET_LOCK();
	TurretData *ftd;
	FOR_EACH_LINK(turrets,ftd,l)
	{
		if(ftd->p == p) td=ftd;
	}
	TURRET_UNLOCK();
	
	return td;
}

local void AddFake(Player *p)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	TurretData *td=findfake(p);
	if(!td)
	{
		td=amalloc(sizeof(TurretData));
		
		LLInit(&td->weaponlist);
		td->p=p;
		td->endtime=0;
		td->tosend=current_ticks();

		td->p->position.x=512;
		td->p->position.y=512;
		td->p->position.xspeed=0;
		td->p->position.yspeed=0;
		td->p->position.rotation=0;
		td->p->position.status=0;
		td->p->position.bounty=13373;
		td->p->position.energy=13373;
		
		td->pos.type=C2S_POSITION;
		td->pos.extra.energy=13373;
		td->pos.extra.shields=1;
		td->pos.extra.super=1;
		td->pos.extra.timer=0;
		td->pos.extra.bursts=1;
		td->pos.extra.repels=2;
		td->pos.extra.portals=3;
		td->pos.extra.decoys=4;
		td->pos.extra.thors=5;
		td->pos.extra.bricks=6;
		td->pos.extra.rockets=7;

		td->pos.weapon.type=W_NULL;
		td->pos.weapon.level=0;
		td->pos.weapon.shrap=0;
		td->pos.weapon.shraplevel=0;
		td->pos.weapon.shrapbouncing=FALSE;
		td->pos.weapon.alternate=FALSE;
		
		updatepos(td);
		TURRET_LOCK();
		LLAdd(&turrets,td);
		TURRET_UNLOCK();
	}
}

local void ChangeFake(Player *p, int ship, int freq, int x, int y, int time)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	TurretData *td=findfake(p);
	if(!td) return;
	
	if(ship != td->p->pkt.ship) game->SetShip(td->p,ship);
	if(freq != td->p->pkt.freq) game->SetFreq(td->p,freq);
	if(x > 0 && x < 1024) td->p->position.x=x*16;
	if(y > 0 && y < 1024) td->p->position.y=y*16;
	if(time) td->endtime=current_ticks()+time*100;
	updatepos(td);
}

local void RemoveFake(Player *p)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	TURRET_LOCK();
	TurretData *td;
	FOR_EACH_LINK(turrets,td,l)
	{
		if(td->p == p) clearturret(td);
	}
	TURRET_UNLOCK();
	
	game->SetShipAndFreq(p,SHIP_SPEC,p->arena->specfreq);
}

local WeaponData* addweapon()
{
	WeaponData *wd=amalloc(sizeof(WeaponData));
	
	wd->id=gwid;
	gwid++;
	
	wd->range=50;
	wd->killfakes=FALSE;
	wd->firedelay=10000;
	
	wd->weapon=W_NULL;
	wd->level=0;
	wd->shrap=0;
	wd->shraplevel=0;
	wd->bounceshrap=FALSE;
	wd->alt=FALSE;
	
	WEAPON_LOCK();
	LLAdd(&weapons,wd);
	WEAPON_UNLOCK();
	
	return wd;
}

local int AddWeapon()
{
	WeaponData *wd=addweapon();
	return wd->id;
}

local void ChangeWeapon(int id, int weapontype, int level, int shrap, int shraplevel, BOOL bounceshrap, BOOL alt, int range, int firedelay, BOOL killfakes)
{
	if(!id) return;
	WeaponData *wd=NULL;
	WEAPON_LOCK();
	WeaponData *fwd;
	FOR_EACH_LINK(weapons,fwd,l)
	{
		if(fwd->id == id) wd=fwd;
	}
	WEAPON_UNLOCK();
	if(!wd) return;
	
	if((weapontype >= 0) && (weapontype < 9)) wd->weapon=weapontype;
	if((level >= 0) && (level < 4)) wd->level=level;
	if((shrap >= 0) && (shrap < 32)) wd->shrap=shrap;
	if((shraplevel >= 0) && (shraplevel < 4)) wd->shraplevel=shraplevel;
	wd->bounceshrap=bounceshrap;
	wd->alt=alt;
	if(range && (range < 1024)) wd->range=range;
	if(firedelay) wd->firedelay=firedelay;
	wd->killfakes=killfakes;
}

local void RemoveWeapon(int id)
{
	if(!id) return;
	WEAPON_LOCK();
	WeaponData *wd;
	FOR_EACH_LINK(weapons,wd,l)
	{
		if(wd->id == id) clearweapon(wd);
	}
	WEAPON_UNLOCK();
}

local void AssignWeapon(Player *p, int id, BOOL use)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	if(!id) return;
	
	BOOL found=FALSE;
	WEAPON_LOCK();
	WeaponData *wd;
	FOR_EACH_LINK(weapons,wd,l)
	{
		if(wd->id == id) found=TRUE;
	}
	WEAPON_UNLOCK();
	if(!found) return;
	
	TURRET_LOCK();
	TurretData *td;
	FOR_EACH_LINK(turrets,td,ll)
	{
		if(td->p == p)
		{
			if(use)
			{
				WeaponInfo *wi=amalloc(sizeof(WeaponInfo));
				wi->id=id;
				wi->tofire=current_ticks();
				LLAdd(&td->weaponlist,wi);
			}
			else
			{
				WeaponInfo *wi;
				FOR_EACH_LINK(td->weaponlist,wi,lll)
				{
					if(wi->id == id) clearweaponinfo(&td->weaponlist,wi);
				}
			}
		}
	}
	TURRET_UNLOCK();
}

local void RemoveWeapons(Player *p)
{
	if(!p) return;
	if(!IS_FAKE(p)) return;
	
	TURRET_LOCK();
	TurretData *td;
	FOR_EACH_LINK(turrets,td,l)
	{
		LLEmpty(&td->weaponlist);
	}
	TURRET_UNLOCK();
}

local void mlfunc()
{
	TURRET_LOCK();
	TurretData *td;
	FOR_EACH_LINK(turrets,td,l)
	{
		ticks_t now=current_ticks();
		
		if(!td->p || !td->p->arena || ((td->endtime != 0) && TICK_GT(now,td->endtime)))
		{
			clearturret(td);
			continue;
		}
		
		updatepos(td);
		
		if(td->p->flags.is_dead)
		{
			if(!td->isdead) td->isdead=TRUE;
			td->tosend+=TICK_DIFF(now,lastml);
			
			WeaponInfo *wi;
			FOR_EACH_LINK(td->weaponlist,wi,l)
			{
				WeaponData *wd=NULL;
				WEAPON_LOCK();
				WeaponData *fwd;
				FOR_EACH_LINK(weapons,fwd,l)
				{
					if(fwd->id == wi->id) wd=fwd;
				}
				WEAPON_UNLOCK();
				
				if(!wd) clearweaponinfo(&td->weaponlist,wi);
				else wi->tofire+=TICK_DIFF(now,lastml);
			}
		}
		else
		{
			if(td->isdead) td->isdead=FALSE;
			
			if(TICK_GT(now,td->tosend))
			{
				td->pos.weapon.type=W_NULL;
				td->pos.time=now;
				game->FakePosition(td->p,&td->pos,sizeof(td->pos));
				td->tosend=now+25;
			}
			
			WeaponInfo *wi;
			FOR_EACH_LINK(td->weaponlist,wi,l)
			{
				WeaponData *wd=NULL;
				WEAPON_LOCK();
				WeaponData *fwd;
				FOR_EACH_LINK(weapons,fwd,l)
				{
					if(fwd->id == wi->id) wd=fwd;
				}
				WEAPON_UNLOCK();
				
				if(!wd) clearweaponinfo(&td->weaponlist,wi);
				else
				{
					if(TICK_GT(now,wi->tofire))
					{
						Player *tp=findTurretTarget(td->p,wd);
						if(tp) td->pos.rotation=fireControl(td->p,wd,tp);
						else continue;
						
						td->pos.weapon.type=wd->weapon;
						td->pos.weapon.level=wd->level;
						td->pos.weapon.shrap=wd->shrap;
						td->pos.weapon.shraplevel=wd->shraplevel;
						td->pos.weapon.shrapbouncing=wd->bounceshrap;
						td->pos.weapon.alternate=wd->alt;
						
						td->pos.time=now;
						game->FakePosition(td->p,&td->pos,sizeof(td->pos));
						wi->tofire=now+wd->firedelay;
					}
				}
			}
		}
	}
	TURRET_UNLOCK();
	lastml=current_ticks();
}

local void NewPlayer(Player *p, int isnew)
{
	if(isnew) return;
	
	if(IS_FAKE(p))
	{
		RemoveWeapons(p);
		RemoveFake(p);
	}
}

local helptext_t cmd_turret=
"Targets: Fake player\n"
"Turret args:\n"
"[sh=#] Ship\n"
"[fr=#] Freq\n"
"[x=#] X position\n"
"[y=#] Y position\n"
"[t=#] Time to exist";

local void turret(const char *command, const char *params, Player *p, const Target *t)
{
	if(t->type != T_PLAYER) chat->SendMessage(p,"Invalid target.");
	else
	{
		if(t->u.p->type != T_FAKE) chat->SendMessage(p,"Only fakes can be turrets.");
		else
		{
			TurretData *td=findfake(t->u.p);
			if(!td)
			{
				AddFake(t->u.p);
				td=findfake(t->u.p);
				if(!td) return;
			}
			
			int ship=td->p->pkt.ship;
			int freq=td->p->pkt.freq;
			if(ship == SHIP_SPEC) ship=SHIP_WARBIRD;
			if(freq == p->arena->specfreq)
			{
				freq=p->pkt.freq;
				td->p->position.x=p->position.x;
				td->p->position.y=p->position.y;
			}
			
			char buf[255];
			char *word=NULL;
			const char *tmp=NULL;
			while(strsplit(params," ,:",buf,sizeof(buf),&tmp))
			{
				word=buf;
				if(!strnicmp(word,"sh=",3))
				{
					word+=3;
					int check=atoi(word);
					if(check)
					{
						check--;
						ship=check;
					}
				}
				else if(!strnicmp(word,"fr=",3))
				{
					word+=3;
					int check=atoi(word);
					if(check >= 0) freq=check;
				}
				else if(!strnicmp(word,"x=",2))
				{
					word+=2;
					int check=atoi(word);
					if(check) td->p->position.x=check*16;
				}
				else if(!strnicmp(word,"y=",2))
				{
					word+=2;
					int check=atoi(word);
					if(check) td->p->position.y=check*16;
				}
				else if(!strnicmp(word,"t=",2))
				{
					word+=2;
					int check=atoi(word);
					if(check) td->endtime=current_ticks()+check*100;
				}
			}
			
			if(ship != td->p->pkt.ship) game->SetShip(td->p,ship);
			if(freq != td->p->pkt.freq) game->SetFreq(td->p,freq);
			updatepos(td);
			
			chat->SendMessage(p,"%s modified.",td->p->name);
		}
	}
}

local helptext_t cmd_changeweapon=
"Targets: Fake player\n"
"Weapon args:\n"
"[r=#] Detection radius\n"
"[f=#] Fire rate\n"
"[-kf] Kill fakes\n"
"[w=#] Weapon\n"
"[l=#] Level\n"
"[s=#] Shrap\n"
"[sl=#] Shrap level\n"
"[-b] Shrap bounce\n"
"[-a] alt fire\n"
"[id=#] Set WeaponID\n"
"[-d] Delete weapon";

local void changeweapon(const char *command, const char *params, Player *p, const Target *t)
{
	WeaponData *wd=NULL;
	int wid=0;
	BOOL tried=FALSE;
	BOOL del=FALSE;
	
	char buf[255];
	char *word=NULL;
	const char *tmp=NULL;
	while(strsplit(params," ,:",buf,sizeof(buf),&tmp))
	{
		word=buf;
		if(!strnicmp(word,"id=",3))
		{
			word+=3;
			int check=atoi(word);
			if(check) wid=check;
		}
		else if(!stricmp(word,"-d"))
		{
			del=TRUE;
		}
	}
	
	if(wid)
	{
		WEAPON_LOCK();
		WeaponData *fwd;
		FOR_EACH_LINK(weapons,fwd,l)
		{
			if(fwd->id == wid) wd=fwd;
		}
		WEAPON_UNLOCK();
		tried=TRUE;
	}
	
	if(del)
	{
		if(!tried) chat->SendMessage(p,"Which WeaponID should be deleted?",wid);
		else if(!wd) chat->SendMessage(p,"Could not find WeaponID #%i to delete.",wid);
		else
		{
			WEAPON_LOCK();
			clearweapon(wd);
			WEAPON_UNLOCK();
			chat->SendMessage(p,"Deleted WeaponID #%i.",wid);
		}
		return;
	}
	
	if(!wd)
	{
		if(tried) chat->SendMessage(p,"Could not find WeaponID #%i. Creating new weapon...",wid);
		wd=addweapon(t->u.p);
		wid=wd->id;
		chat->SendMessage(p,"Created WeaponID #%i. Use '/?faw id=%i' to assign it.",wid,wid);
	}
	
	tmp=NULL;
	while(strsplit(params," ,:",buf,sizeof(buf),&tmp))
	{
		word=buf;
		if(!strnicmp(word,"f=",2))
		{
			word+=2;
			int check=atoi(word);
			if(check) wd->firedelay=check;
		}
		else if(!strnicmp(word,"r=",2))
		{
			word+=2;
			int check=atoi(word);
			if(check) wd->range=check;
		}
		else if(!stricmp(word,"-kf"))
		{
			wd->killfakes=!wd->killfakes;
		}
		else if(!strnicmp(word,"w=",2))
		{
			word+=2;
			int check=atoi(word);
			if(check) wd->weapon=check;
		}
		else if(!strnicmp(word,"l=",2))
		{
			word+=2;
			int check=atoi(word);
			if(check)
			{
				check--;
				wd->level=check;
			}
		}
		else if(!strnicmp(word,"s=",2))
		{
			word+=2;
			int check=atoi(word);
			if(check >= 0) wd->shrap=check;
		}
		else if(!strnicmp(word,"sl=",3))
		{
			word+=3;
			int check=atoi(word);
			if(check)
			{
				check--;
				wd->shraplevel=check;
			}
		}
		else if(!stricmp(word,"-b"))
		{
			wd->bounceshrap=!wd->bounceshrap;
		}
		else if(!stricmp(word,"-a"))
		{
			wd->alt=!wd->alt;
		}
	}
	chat->SendMessage(p,"Changed WeaponID #%i.",wid);
}

local helptext_t cmd_assignweapon=
"Targets: Fake player\n"
"Args: <id=#> [-u]\n"
"Assigns a weapon to a turret, or -u to unassign.";

local void assignweapon(const char *command, const char *params, Player *p, const Target *t)
{
	if(t->type != T_PLAYER) chat->SendMessage(p,"Invalid target.");
	else
	{
		if(t->u.p->type != T_FAKE) chat->SendMessage(p,"Only fakes can be turrets.");
		else
		{
			int wid=0;
			BOOL use=TRUE;
			
			char buf[255];
			char *word=NULL;
			const char *tmp=NULL;
			while(strsplit(params," ,:",buf,sizeof(buf),&tmp))
			{
				word=buf;
				if(!strnicmp(word,"id=",3))
				{
					word+=3;
					int check=atoi(word);
					if(check) wid=check;
				}
				else if(!stricmp(word,"-u"))
				{
					use=FALSE;
				}
			}
			
			if(!wid)
			{
				chat->SendMessage(p,"Which weapon should %s %s?",t->u.p->name,(use ? "use" : "stop using"));
				return;
			}
			else
			{
				AssignWeapon(t->u.p,wid,use);
				chat->SendMessage(p,"%s is now using weapon #%i.",t->u.p->name,wid);
			}
		}
	}
}

local helptext_t cmd_killturret=
"Targets: Fake player\n"
"Args: none\n"
"Kills turret.";

local void killturret(const char *command, const char *params, Player *p, const Target *t)
{
	if(t->type != T_PLAYER) chat->SendMessage(p,"Invalid target.");
	else
	{
		if(t->u.p->type != T_FAKE) chat->SendMessage(p,"Only fakes can be turrets.");
		else
		{
			RemoveWeapons(t->u.p);
			RemoveFake(t->u.p);
			
			chat->SendMessage(p,"%s is no longer a turret.",t->u.p->name);
		}
	}
}

local helptext_t cmd_killturrets=
"Targets: none\n"
"Args: [name]\n"
"Kills all turrets, or just those starting with [name].";

local void killturrets(const char *command, const char *params, Player *p, const Target *t)
{
	TURRET_LOCK();
	TurretData *td;
	FOR_EACH_LINK(turrets,td,l)
	{
		if(!params || strnicmp(td->p->name,params,strlen(params))) clearturret(td);
	}
	TURRET_UNLOCK();
	
	if(p) chat->SendMessage(p,"Killed all turrets.");
	else
	{
		WEAPON_LOCK();
		WeaponData *wd;
		FOR_EACH_LINK(weapons,wd,l)
		{
			clearweapon(wd);
		}
		WEAPON_UNLOCK();
	}
}

local helptext_t cmd_fakehelp=
"Targets: none\n"
"Args: none\n"
"Displays commands dealing with fake players.";

local void fakehelp(const char *command, const char *params, Player *p, const Target *t)
{
	chat->SendMessage(p,"If all fake modules are loaded, the following commands will be available:");
	chat->SendMessage(p,"(makefake)      ?makefake <name>");
	chat->SendMessage(p,"(makefake)     /?killfake");
	chat->SendMessage(p,"(faketurret)   /?ft [sh=#] [fr=#] [x=#] [y=#] [t=#]");
	chat->SendMessage(p,"(changeweapon)  ?fcw [r=#] [f=#] [-kf] [w=#] [l=#] [s=#] [sl=#] [-b] [-a] [id=#] [-d]");
	chat->SendMessage(p,"(assignweapon) /?faw <id>");
	chat->SendMessage(p,"(killturret)   /?fkt");
	chat->SendMessage(p,"(killturrets)   ?fkts [name]");
	chat->SendMessage(p,"(fakeenergy)   /?fe [nrg=#] [chg=#] [spx=#] [spy=#] [spr=#]");
	chat->SendMessage(p,"(fakemove)     /?fm [spd=#] [thr=#] [x=#] [y=#] [p=name] [-w]");
	chat->SendMessage(p,"(killmove)     /?fkm");
	chat->SendMessage(p,"(killallmove)   ?fkam [name]");
	chat->SendMessage(p,"(fakeai)       /?fai [p=name] [r=#] [-c]");
	chat->SendMessage(p,"For help on a specific command, type ?man <command>");
}

local Ifakeweapons weaponsint=
{
	INTERFACE_HEAD_INIT(I_FAKEWEAPONS,"fakeweapons")
	
	AddFake, ChangeFake, RemoveFake,
	AddWeapon, ChangeWeapon, RemoveWeapon,
	AssignWeapon, RemoveWeapons, 
};

EXPORT const char info_fakeweapons[]=
".\n"
"Fake Weapons Module\n"
"by Cheese\n"
"Makes fake players fire weapons.";

EXPORT int MM_fakeweapons(int action, Imodman *mm2, Arena *a)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN,ALLARENAS);
		pd=mm->GetInterface(I_PLAYERDATA,ALLARENAS);
		cmd=mm->GetInterface(I_CMDMAN,ALLARENAS);
		chat=mm->GetInterface(I_CHAT,ALLARENAS);
		game=mm->GetInterface(I_GAME,ALLARENAS);
		cfg=mm->GetInterface(I_CONFIG,ALLARENAS);
		//fake=mm->GetInterface(I_FAKE,ALLARENAS);
		if(!lm || !pd || !cmd || !game || !cfg) return MM_FAIL;
		
		/*nrg=mm->GetInterface(I_FAKEENERGY,ALLARENAS);
		if(!nrg) lm->Log(L_ERROR,"<fakeweapons> Could not find fakeenergy, bots will not have energy.");
		move=mm->GetInterface(I_FAKEMOVE,ALLARENAS);
		if(!move) lm->Log(L_ERROR,"<fakeweapons> Could not find fakemove, bots will be stationary.");*/
		
		mm->RegInterface(&weaponsint,ALLARENAS);
		
		mm->RegCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->RegCallback(CB_NEWPLAYER,NewPlayer,ALLARENAS);
		
		cmd->AddCommand("ft",turret,ALLARENAS,cmd_turret);
		cmd->AddCommand("fcw",changeweapon,ALLARENAS,cmd_changeweapon);
		cmd->AddCommand("faw",assignweapon,ALLARENAS,cmd_assignweapon);
		cmd->AddCommand("fkt",killturret,ALLARENAS,cmd_killturret);
		cmd->AddCommand("fkts",killturrets,ALLARENAS,cmd_killturrets);
		cmd->AddCommand("fakehelp",fakehelp,ALLARENAS,cmd_fakehelp);
		
		lm->Log(L_ERROR,"<fake> Fake Weapons Module has loaded.");
		
		return MM_OK;
	}
	else if(action == MM_ATTACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake Weapons Module has attached to the arena.");
        
		return MM_OK;
	}
	else if(action == MM_DETACH)
	{
		lm->LogA(L_ERROR,"fake",a,"Fake Weapons Module has detached from the arena.");
        
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		lm->Log(L_ERROR,"<fake> Fake Weapons Module has unloaded.");
		
		if(mm->UnregInterface(&weaponsint,ALLARENAS)) return MM_FAIL;
		
		cmd->RemoveCommand("ft",turret,ALLARENAS);
		cmd->RemoveCommand("fcw",changeweapon,ALLARENAS);
		cmd->RemoveCommand("faw",assignweapon,ALLARENAS);
		cmd->RemoveCommand("fkt",killturret,ALLARENAS);
		cmd->RemoveCommand("fkts",killturrets,ALLARENAS);
		cmd->RemoveCommand("fakehelp",fakehelp,ALLARENAS);
		
		mm->UnregCallback(CB_MAINLOOP,mlfunc,ALLARENAS);
		mm->UnregCallback(CB_NEWPLAYER,NewPlayer,ALLARENAS);
		
		killturrets(NULL,NULL,NULL,NULL);
		LLEmpty(&turrets);
		LLEmpty(&weapons);
		
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(game);
		mm->ReleaseInterface(cfg);
		//mm->ReleaseInterface(fake);
		
		//mm->ReleaseInterface(nrg);
		//mm->ReleaseInterface(move);
		
		return MM_OK;
	}
	return MM_FAIL;
}
